<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

       
  	


<!-- Common Java Script -->

<!-- ===== JS =====-->
    <!-- jQuery-->
       <script src="js/jquery-latest.js"></script>
   	   <script src="js/chosen.jquery.min.js"></script>
	   <script src="js/jquery.min.js"></script>
       <script src="js/jquery-migrate.min.js"></script>
    <!--   -->  
      
    <!-- General-->
		<script src="js/bootstrap.min.js"></script>
		<script src="js/plugins.js"></script>
		<script src="js/general.js"></script>
		<script src="js/jquery.select2.min.js"></script>
		<script src="js/jquery.validate.min.js"></script>
		<script src="js/jquery.asonWidget.js"></script>
		<script src="js/modernizr.min.js"></script>
	
	<!--  -->	
	
	<!-- Semi general-->
	    <script type="text/javascript">
	      var paceSemiGeneral = { restartOnPushState: false };
	      if (typeof paceSpecific != 'undefined'){
	      	var paceOptions = $.extend( {}, paceSemiGeneral, paceSpecific );
	      	paceOptions = paceOptions;
	      }else{
	      	paceOptions = paceSemiGeneral;
	      }
	      
	   </script>
    	<script src="js/pace.min.js"></script>
		
	<link href="${pageContext.request.contextPath}/css/basic.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath}/css/general.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath}/css/select2.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath}/css/theme.css" rel="stylesheet" type="text/css">
	
	
	
	