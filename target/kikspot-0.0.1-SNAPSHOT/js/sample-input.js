/*
 * Created By Bhagya On October 26th, 2015
 * Javascript functions for checking sample input and output for mobile services
 */


/***
 * Function For verify the facebook login user
 */
function socialLogin() {
	 $.ajax({
	    url: "/kikspot/mobile/sociallogin.do",
	    type: 'POST',
	    data: JSON.stringify({ emailId: "pharistajivan@gmail.com", firstName:"sai",lastName:"ram"}),
	    contentType: 'application/json',
	 	success: function(data) {
	        alert(data);
	    },
	    error:function(data,status,er) {
	        alert("error: "+data+" status: "+status+" er:"+er);
	    }
	});
}



function signup() {
	 $.ajax({
	    url: "/kikspot/mobile/signup.do",
	    type: 'POST',
	    data: JSON.stringify({ emailId: "jeevan@knstek.com", username:"test",password:"123"}),
	    contentType: 'application/json',
	 	success: function(data) {
	        alert(data);
	    },
	    error:function(data,status,er) {
	        alert("error: "+data+" status: "+status+" er:"+er);
	    }
	});
}


function completeSignup() {
	 $.ajax({
	    url: "/kikspot/mobile/completesignup.do",
	    type: 'POST',
	    data: JSON.stringify({
	    		userId: 1,
	    		firstName:"jeevan",
	    		lastName:"mysore",
	    		dateofBirth:"12/12/1988",
	    		address1:"48/1",
	    		address2: "tavarekere",
	    		city: "Bangalore",
	    		state: "Karnataka",
	    		zip : "560029"
	    	}),
	    contentType: 'application/json',
	 	success: function(data) {
	        alert(data);
	    },
	    error:function(data,status,er) {
	        alert("error: "+data+" status: "+status+" er:"+er);
	    }
	});
}

function bypassUser(){
	 $.ajax({
		    url: "/kikspot/mobile/bypassuser.do",
		    type: 'GET',
		    
		    contentType: 'application/json',
		 	success: function(data) {
		        alert(data);
		    },
		    error:function(data,status,er) {
		        alert("error: "+data+" status: "+status+" er:"+er);
		    }
		});
}


function loginUser() {
	 $.ajax({
	    url: "/kikspot/mobile/login.do",
	    type: 'POST',
	    data: JSON.stringify(
	    		{ 
	    			username:"test",
	    			password:"123"
	    		}
	    	),
	    contentType: 'application/json',
	 	success: function(data) {
	        alert(data);
	    },
	    error:function(data,status,er) {
	        alert("error: "+data+" status: "+status+" er:"+er);
	    }
	});
}

/**
 * Created By Bhagya On November 23rd,2015
 * Function for getting Basic details of near by locations based on lat and lng
 */
function getLocations() {
	 $.ajax({
	    url: "/kikspot/mobile/getrecommendationlocations.do",
	    type: 'POST',
	    data: JSON.stringify({ userId: 3, latitude:-33.867217,longitude:151.195939}),
	    contentType: 'application/json',
	 	success: function(data) {
	        alert(data);
	    },
	    error:function(data,status,er) {
	        alert("error: "+data+" status: "+status+" er:"+er);
	    }
	});
}
/**
 * Created By Bhagya On November 25th, 2015
 * Function For get the location Dtails By Location APIID
 */
function getLocationDetailsByLocationAPIId() {
	 $.ajax({
	    url: "/kikspot//mobile/getrecommendationlocationdetails.do",
	    type: 'POST',
	    data: JSON.stringify({ locationAPIId:"ChIJIfBAsjeuEmsRdgu9Pl1Ps48"}),
	    contentType: 'application/json',
	 	success: function(data) {
	        alert(data);
	    },
	    error:function(data,status,er) {
	        alert("error: "+data+" status: "+status+" er:"+er);
	    }
	});
}
