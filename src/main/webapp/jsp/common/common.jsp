<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<link rel="icon" href="${pageContext.request.contextPath}/image/fav_icon.png" type="image/png" sizes="16x16">

<!-- Common Java Script -->

<!-- ===== JS =====-->
    <!-- jQuery-->
       <script src="${pageContext.request.contextPath}/js/jquery-latest.js"></script>
   	   <script src="${pageContext.request.contextPath}/js/chosen.jquery.min.js"></script>
	   <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
       <script src="${pageContext.request.contextPath}/js/jquery-migrate.min.js"></script>
    <!--   -->  
      
    <!-- General-->
		<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
		<script src="${pageContext.request.contextPath}/js/plugins.js"></script>
		<script src="${pageContext.request.contextPath}/js/general.js"></script>
		<script src="${pageContext.request.contextPath}/js/jquery.select2.min.js"></script>
		<script src="${pageContext.request.contextPath}/js/jquery.validate.min.js"></script>
		<script src="${pageContext.request.contextPath}/js/jquery.asonWidget.js"></script>
		<script src="${pageContext.request.contextPath}/js/modernizr.min.js"></script>
	
	<!--  -->	
	
	<!-- Semi general-->
	    <script type="text/javascript">
	      var paceSemiGeneral = { restartOnPushState: false };
	      if (typeof paceSpecific != 'undefined'){
	      	var paceOptions = $.extend( {}, paceSemiGeneral, paceSpecific );
	      	paceOptions = paceOptions;
	      }else{
	      	paceOptions = paceSemiGeneral;
	      }
	      
	   </script>
    	<script src="${pageContext.request.contextPath}/js/pace.min.js"></script>
    	
    	
    	
    <!-- Common CSS -->
		
	<link href="${pageContext.request.contextPath}/css/basic.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath}/css/general.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath}/css/select2.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath}/css/theme.css" rel="stylesheet" type="text/css">
	
	
	
	