<!-- Created By Bhagya On October 27th,2015
	Common header Jsp Page -->

<header class="l-header l-header-1 t-header-1">
          <div class="navbar navbar-ason">
            <div class="container-fluid">
              <div class="navbar-header">
             	<button type="button" data-toggle="collapse" data-target="#ason-navbar-collapse" class="navbar-toggle collapsed"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>   <a href="index.html" class="navbar-brand widget-logo">Kikspot</a>
              </div>
              <div id="ason-navbar-collapse" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                  <li>
                    <!-- Search Widget-->
                    <div class="widget-search search-in-header is-search-right t-search-1">
                      <form role="form" action="#">
                        <input type="text" placeholder="Search..." class="form-control">
                        <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                      </form>
                    </div>
                  </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                  <li>
                    <!-- Profile Widget-->
                    <div class="widget-profile profile-in-header">
                      <button type="button" data-toggle="dropdown" class="btn dropdown-toggle"><span class="name">Arunkumar</span><img src="${pageContext.request.contextPath}/image/avatar.jpg"></button>
                      <ul role="menu" class="dropdown-menu">
                                               
                        <li class="power"><a href="login.html"><i class="fa fa-power-off"></i>Sign Out</a></li>
                      </ul>
                    </div>
                  </li>
                
                </ul>
              </div>
            </div>
          </div>
        </header>