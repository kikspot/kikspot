 <!-- common Left Side Navigation Bar -->
 
 <!--Left Sidebar Content-->
      <aside id="sb-left" class="l-sidebar l-sidebar-1 t-sidebar-1">
        <!--Switcher-->
        <div class="l-side-box"><a href="#" data-ason-type="sidebar" data-ason-to-sm="sidebar" data-ason-target="#sb-left" class="sidebar-switcher switcher t-switcher-side ason-widget"><i class="fa fa-bars"></i></a></div>
        <div class="l-side-box">
          <!--Logo-->
          <div class="widget-logo logo-in-side">
            <h1 ><a href="index.html" style="color:#FFFFFF">Kikspot</a></h1>
          </div>
        </div>
        <!--Main Menu-->
        <div class="l-side-box">
          <!--MAIN NAVIGATION MENU-->
          <nav class="navigation">
            <ul data-ason-type="menu" class="ason-widget ason-nav">
              <li class="active"><a href="#"><i class="icon fa fa-dashboard"></i><span class="title">Dashboard</span></a></li>
              <li><a href="#"><i class="icon fa fa-sitemap"></i><span class="title">User Management</span><span class="arrow"><i class="fa fa-angle-left"></i></span></a>
              	<ul>
              		<li><a href="${pageContext.request.contextPath}/admin/viewusers.do"><span class="title">Users</span></a></li>
              	</ul>
              </li>
              <li><a href="table.html"><i class="icon fa fa-gamepad"></i><span class="title">Game Management</span></a></li>
              <li><a href="#"><i class="icon fa fa-file-text"></i><span class="title">Reports</span></a></li>
              <li><a href="#"><i class="icon fa fa-sitemap"></i><span class="title">Others</span><span class="arrow"><i class="fa fa-angle-left"></i></span></a>
                <ul>
                  <li><a href="#"><span class="title">Others 1</span></a>
                  </li>
                  <li><a href="#"><span class="title">Others 2</span></a>
                    
                  </li>
                  <li><a href="#"><span class="title">Others 3</span></a>
                  </li>
                  <li><a href="#"><span class="title">Others 4</span></a>
                  </li>
                </ul>
              </li>
			  </ul>
          </nav>
        </div>
      </aside>