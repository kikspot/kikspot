<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- Created By Bhagya on Dec 03rd,2015
	View Users For Admin -->
<html>
<head>
<title>View Users</title>
	<%@ include file="/jsp/common/common.jsp"%>
</head>
<body>
<body class="l-dashboard  l-footer-sticky-1  ">
    <!--SECTION-->
    <section class="l-main-container">
    
      <!--Left Side Navigation Content-->
     <%@ include file="/jsp/common/leftSideNavigation.jsp"%>
     
	     <!--Main Content-->
	      <section class="l-container">
	      
	        <!--HEADER-->
	        <%@ include file="/jsp/common/header.jsp"%>
	        
	        <!-- Middle Content Start -->
			<div>
				<div class="l-spaced">
        
        <div class="resp-tab-content resp-tab-content-active" aria-labelledby="tab_item-0" style="display:block">
                <div class="l-row l-spaced-bottom">
                  <div class="l-box">
                    <div class="l-box-header">
                      <h2 class="l-box-title"><span>Users</span></h2>
                      <ul class="l-box-options">
                        <li><a href="#"><i class="fa fa-cogs"></i></a></li>
                        <li><a href="#" data-ason-type="fullscreen" data-ason-target=".l-box" data-ason-content="true" class="ason-widget"><i class="fa fa-expand"></i></a></li>
                        <li><a href="#" data-ason-type="refresh" data-ason-target=".l-box" data-ason-duration="1000" class="ason-widget"><i class="fa fa-rotate-right"></i></a></li>
                        <li><a href="#" data-ason-type="toggle" data-ason-find=".l-box" data-ason-target=".l-box-body" data-ason-content="true" data-ason-duration="200" class="ason-widget"><i class="fa fa-chevron-down"></i></a></li>
                        <li><a href="#" data-ason-type="delete" data-ason-target=".l-box" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"><i class="fa fa-times"></i></a></li>
                      </ul>
                    </div>
                    <div class="l-box-body">
                      <div id="dataTableId_wrapper" class="dataTables_wrapper table-responsive"><div class="dataTables_length" id="dataTableId_length"><label>Show <select name="dataTableId_length" aria-controls="dataTableId" class=""><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div><div id="dataTableId_filter" class="dataTables_filter"><label>Search:<input type="search" class="" placeholder="" aria-controls="dataTableId"></label></div>
					  
					  <table id="dataTableId" cellspacing="0" width="100%" class="display dataTable table" role="grid" aria-describedby="dataTableId_info" style="width: 100%;">
                        <thead>
                          <tr role="row">
	                          <th class="sorting" style="width: 165px;">User Name</th>
	                          <th class="sorting" style="width: 232px;">Email</th>
	                          <th class="sorting" style="width: 123px;">Street</th>
	                          <th class="sorting" style="width: 44px;">City</th>
	                          <th class="sorting" style="width: 102px;">State</th>
	                          <th class="sorting" style="width: 99px;">Country</th>
	                          <th class="sorting" style="width: 99px;">Pin</th>
	                          <th class="sorting" style="width: 99px;">User Role</th>
	                          <th class="sorting" style="width: 99px;">User Type</th>
	                          <th class="sorting" style="width: 120px;">Account Creation Date</th>
                          </tr>
                        </thead>
                        <!-- <tfoot>
                          <tr><th rowspan="1" colspan="1">Name</th><th rowspan="1" colspan="1">Position</th><th rowspan="1" colspan="1">Office</th><th rowspan="1" colspan="1">Age</th><th rowspan="1" colspan="1">Start date</th><th rowspan="1" colspan="1">Salary</th></tr>
                        </tfoot> -->
                        <tbody>
                        	<c:forEach var="kikspotUser" items="${kikspotUsers}">
	                      		<tr>
	                      			<td>${kikspotUser.firstName} ${kikspotUser.lastName}</td>
	                      			<td>${kikspotUser.emailId}</td>
	                      			<td>${kikspotUser.address2}</td>
	                      			<td>${kikspotUser.city}</td>
	                      			<td>${kikspotUser.state}</td>
	                      			<td>${kikspotUser.country}</td>
	                      			<td>${kikspotUser.zip}</td>
	                      			<td>${kikspotUser.userRole}</td>
	                      			<td>${kikspotUser.userType}</td>
	                      			<td><fmt:formatDate pattern="MM/dd/yyyy HH:mm" value="${kikspotUser.accountCreationDate}" /></td>
	                      		</tr>    
                      		</c:forEach>
						  
						</tbody>
                      </table>
                     
                      <div class="dataTables_info" id="dataTableId_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div>
                      <div class="dataTables_paginate paging_simple_numbers" id="dataTableId_paginate">
	                      <a class="paginate_button previous disabled" aria-controls="dataTableId" data-dt-idx="0" tabindex="0" id="dataTableId_previous">Previous</a>
	                      <span>
		                      <a class="paginate_button current" aria-controls="dataTableId" data-dt-idx="1" tabindex="0">1</a>
		                      <a class="paginate_button " aria-controls="dataTableId" data-dt-idx="2" tabindex="0">2</a>
		                      <a class="paginate_button " aria-controls="dataTableId" data-dt-idx="3" tabindex="0">3</a>
		                      <a class="paginate_button " aria-controls="dataTableId" data-dt-idx="4" tabindex="0">4</a>
		                      <a class="paginate_button " aria-controls="dataTableId" data-dt-idx="5" tabindex="0">5</a>
		                      <a class="paginate_button " aria-controls="dataTableId" data-dt-idx="6" tabindex="0">6</a>
	                      </span>
	                      <a class="paginate_button next" aria-controls="dataTableId" data-dt-idx="7" tabindex="0" id="dataTableId_next">Next</a>
                      </div>
                     </div>
                    </div>
                  </div>
                </div>
              </div>
          
        </div>
			</div>
			
			<!--FOOTER-->
	        <%@ include file="/jsp/common/footer.jsp"%>
	        
	       </section> <!--End of Main Content section -->
     
     </section> <!-- End of Main Container Section -->

</body>
</html>