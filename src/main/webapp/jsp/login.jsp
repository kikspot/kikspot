
<!DOCTYPE html>
<html lang="en">
  
<head>
    <title>Kikspot - login</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    
    <!-- ===== CSS =====-->
    <%@ include file="/jsp/common/common.jsp"%>
  </head>
  <body class="login-bg">
    
    <!--SECTION-->
    <section class="l-main-container">
      <!--Main Content-->
      <div class="login-wrapper">
        <div class="login-container">
          <!--Logo-->
          <h1 class="login-logo"> <img src="${pageContext.request.contextPath}/image/logo.png" /></h1>
          <c:if test="${ not empty error}">
			<div id="login_head_text" style="position: relative;top: -20px;font-family: serif;color: red;font-size: 18px;text-align:center;">
				${error}
			</div>
		  </c:if>
		  <c:if test="${ not empty sucessMessage}">
		     <div id="login_head_text" style="position: relative;top: -120px;font-family: serif;color: green;font-size: 18px;text-align:center;">
					${sucessMessage}
			 </div>
		  </c:if>
		  <c:if test="${ not empty errorMessage}">
			<div id="login_head_text" style="position: relative;top: -20px;font-family: serif;color: red;font-size: 18px;text-align:center;">
				${errorMessage}
			</div>
		  </c:if>
          <!--Login Form-->
          <form id="loginForm" class="login-form"  action="<c:url value= '/login' />" method='POST'>
            <div class="form-group">
              <input id="loginEmail" type="text" name="username" placeholder="Email" class="form-control">
            </div>
            <div class="form-group">
              <input id="loginPassword" type="password" name="password" placeholder="Password" class="form-control">
            </div>
            <button type="submit" class="btn btn-dark btn-block btn-login">Sign In</button>
            <div class="login-social">
             
            </div>
            <div class="login-options"><a href="/kikspot/forgotpassword.do" class="fl">FORGOT PASSWORD ?</a></div>
          </form>
        </div>
      </div>
    </section>
    <!-- ===== JS =====-->
    
 
  </body>

</html>