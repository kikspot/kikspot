<html>
<head>
<title>Forgot Password</title>
<style>
.forgotpassword .content .input {
	width: 280px;
	padding: 15px 25px;
	
	font-family: sans-serif;
	font-weight: 400;
	font-size: 14px;
	color: #9d9e9e;
	text-shadow: 1px 1px 0 rgba(256,256,256,1.0);
	
	background: #fff;
	border: 1px solid #fff;
	border-radius: 5px;
	
	box-shadow: inset 0 1px 3px rgba(0,0,0,0.50);
	-moz-box-shadow: inset 0 1px 3px rgba(0,0,0,0.50);
	-webkit-box-shadow: inset 0 1px 3px rgba(0,0,0,0.50);
	position : relative;
	z-index : 9999;
    behavior: url(js/js/PIE.htc);
    
}
.label{
	font-size: 18px;
}
.login_button{
	    padding: 10px 10px 10px 10px;
	    background-color: #7ec0ee;
}
</style>
</head>
<body>
	
	<form name='forgotpassword' class="forgotpassword" action="${pageContext.request.contextPath}/forgotpassword.do"  method='POST'>  
   			<div class="content" style="text-align:center;margin-top: 200px;">
   				
   					<p class="text-center" >
					<strong>To reset your password, enter the email
							address you use to sign in to Kikspot.</strong>
					</p>
				
	   			<label>Email</label>
	   			<input name="emailId" type="text" class="input"  required="required" value="EmailId" 
	   					id="email"  /><br><br>
	   					
	   			
				<div style="text-align: right;margin-right: 430px; margin-top: 34px;">
					<input name="submit" type="submit"  class="login_button" value="Submit" /> 
				</div>   
				 
   			</div>
 		</form>  
</body>
</html>