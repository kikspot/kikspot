<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Kikspot Dashboard</title>
	<%@ include file="/jsp/common/common.jsp"%>
</head>
<body>
<body class="l-dashboard  l-footer-sticky-1  ">
    <!--SECTION-->
    <section class="l-main-container">
    
      <!--Left Side Navigation Content-->
     <%@ include file="/jsp/common/leftSideNavigation.jsp"%>
     
	     <!--Main Content-->
	      <section class="l-container">
	      
	        <!--HEADER-->
	        <%@ include file="/jsp/common/header.jsp"%>
	        
	        <!-- Middle Content Start -->
			<div>
				<h3>Dashboard Page</h3>
			</div>
			
			<!--FOOTER-->
	        <%@ include file="/jsp/common/footer.jsp"%>
	        
	       </section> <!--End of Main Content section -->
     
     </section> <!-- End of Main Container Section -->

</body>
</html>