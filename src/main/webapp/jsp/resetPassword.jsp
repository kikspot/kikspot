  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  

<html>  
<head>
<title>Kikspot - ResetPassword</title>
<style>  
	.resetpassword .content .input {
	width: 280px;
	padding: 15px 25px;
	
	font-family: sans-serif;
	font-weight: 400;
	font-size: 14px;
	color: #9d9e9e;
	text-shadow: 1px 1px 0 rgba(256,256,256,1.0);
	
	background: #fff;
	border: 1px solid #fff;
	border-radius: 5px;
	
	box-shadow: inset 0 1px 3px rgba(0,0,0,0.50);
	-moz-box-shadow: inset 0 1px 3px rgba(0,0,0,0.50);
	-webkit-box-shadow: inset 0 1px 3px rgba(0,0,0,0.50);
	position : relative;
	z-index : 9999;
    behavior: url(js/js/PIE.htc);
    
}
.label{
	font-size: 18px;
}
.login_button{
	    padding: 10px 10px 10px 10px;
	    background-color: #7ec0ee;
}
</style>  
</head>
<body>
	<h3 style="text-align: center;margin-top:200px;font-size: 30px;">Reset Password</h3>  
 	 <form name='resetpassword' class="resetpassword" action="${pageContext.request.contextPath}/resetpassword.do"  method='POST'>  
   			<div class="content" style="text-align:center;">
   					<input type="hidden" name="userId" value="${userId}">
	   			<label>Password</label>
	   			<input id="text_password" name="password" class="input password" style="margin-left: 55px;margin-top: 4px;" value="Password" type="text" /><br>
	   			
	   			<label>Confirm Password</label>
	   			<input id="text_password" name="confirmPassword" class="input password" style="margin-top: 10px;" value="Confirm Password" type="text" /><br>
	   			
				<div style="text-align: right;margin-right: 430px; margin-top: 34px;">
					<input name="submit" type="submit"  class="login_button" value="Reset Password" /> 
				</div>   
				 
   			</div>
 		</form>  
</body>
</html>