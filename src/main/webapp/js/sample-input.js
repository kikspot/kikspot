/*
 * Created By Bhagya On October 26th, 2015
 * Javascript functions for checking sample input and output for mobile services
 */


/***
 * Function For verify the facebook login user
 */
function socialLogin() {
	 $.ajax({
	    url: "/kikspot/mobile/sociallogin.do",
	    type: 'POST',
	    data: JSON.stringify({ emailId: "jkmysore12@gmail.com", firstName:"sai",lastName:"ram"}),
	    contentType: 'application/json',
	 	success: function(data) {
	        alert(data);
	    },
	    error:function(data,status,er) {
	        alert("error: "+data+" status: "+status+" er:"+er);
	    }
	});
}



function signup() {
	 $.ajax({
	    url: "/kikspot/mobile/signup.do",
	    type: 'POST',
	    data: JSON.stringify({ emailId: "firdous@knstek.com", username:"test1",password:"123"}),
	    contentType: 'application/json',
	 	success: function(data) {
	        alert(data);
	    },
	    error:function(data,status,er) {
	        alert("error: "+data+" status: "+status+" er:"+er);
	    }
	});
}


function completeSignup() {
	 $.ajax({
	    url: "/kikspot/mobile/completesignup.do",
	    type: 'POST',
	    data: JSON.stringify({
	    		userId: 1,
	    		firstName:"Firdous",
	    		lastName:"Tabassum",
	    		dateofBirth:"04/10/1992",
	    		address1:"19/25",
	    		address2: "Bannerghatta Road",
	    		city: "Bangalore",
	    		state: "Karnataka",
	    		zip : "560029"
	    	}),
	    contentType: 'application/json',
	 	success: function(data) {
	        alert(data);
	    },
	    error:function(data,status,er) {
	        alert("error: "+data+" status: "+status+" er:"+er);
	    }
	});
}

function bypassUser(){
	 $.ajax({
		    url: "/kikspot/mobile/bypassuser.do",
		    type: 'GET',
		    
		    contentType: 'application/json',
		 	success: function(data) {
		        alert(data);
		    },
		    error:function(data,status,er) {
		        alert("error: "+data+" status: "+status+" er:"+er);
		    }
		});
}


function loginUser() {
	 $.ajax({
	    url: "/kikspot/mobile/login.do",
	    type: 'POST',
	    data: JSON.stringify(
	    		{ 
	    			username:"kikspot",
	    			password:"kikspot123"
	    		}
	    	),
	    contentType: 'application/json',
	 	success: function(data) {
	        alert(data);
	    },
	    error:function(data,status,er) {
	        alert("error: "+data+" status: "+status+" er:"+er);
	    }
	});
}

/**
 * Created By Bhagya On November 23rd,2015
 * Function for getting Basic details of near by locations based on lat and lng
 */
function getLocations() {
	 $.ajax({
	    url: "/kikspot/mobile/getrecommendationlocations.do",
	    type: 'POST',
	    data: JSON.stringify({ userId: 3, latitude:-33.867217,longitude:151.195939}),
	    contentType: 'application/json',
	 	success: function(data) {
	        alert(data);
	    },
	    error:function(data,status,er) {
	        alert("error: "+data+" status: "+status+" er:"+er);
	    }
	});
}
/**
 * Created By Bhagya On November 25th, 2015
 * Function For get the location Dtails By Location APIID
 */
function getLocationDetailsByLocationAPIId() {
	 $.ajax({
	    url: "/kikspot//mobile/getrecommendationlocationdetails.do",
	    type: 'POST',
	    data: JSON.stringify({ locationAPIId:"ChIJIfBAsjeuEmsRdgu9Pl1Ps48"}),
	    contentType: 'application/json',
	 	success: function(data) {
	        alert(data);
	    },
	    error:function(data,status,er) {
	        alert("error: "+data+" status: "+status+" er:"+er);
	    }
	});
}



function userDashboard(){
	 $.ajax({
		    url: "/kikspot/mobile/dashboard.do",
		    type: 'POST',
		    data: JSON.stringify({
		    	userId:24
		    		}),
		    contentType: 'application/json',
		 	success: function(data) {
		        alert(data);
		    },
		    error:function(data,status,er) {
		        alert("error: "+data+" status: "+status+" er:"+er);
		    }
		});
}


/**
 * Created By Bhagya On November 26th,2015
 * Function For edit Profile
 */
function editProfile() {
	 $.ajax({
	    url: "/kikspot/mobile/editprofile.do",
	    type: 'POST',
	    data: JSON.stringify({
	    		userId: 1,
	    		firstName:"jeevan",
	    		lastName:"mysore",
	    		dateofBirth:"12/12/1988",
	    		address1:"48/1",
	    		address2: "tavarekere",
	    		city: "Bangalore",
	    		state: "Karnataka",
	    		zip : "560029"
	    	}),
	    contentType: 'application/json',
	 	success: function(data) {
	        alert(data);
	    },
	    error:function(data,status,er) {
	        alert("error: "+data+" status: "+status+" er:"+er);
	    }
	});
}

/**
 * 
 *  Created by Firdous on 26th November 2015
 *  Functions to handle feedback
 *  
 */

function getfeedback() {
	$.ajax({
		type : 'POST',
		url : "/kikspot/mobile/feedback.do",
		data : JSON.stringify({
		
			feedbackMessage:"I like this application.. This is very useful for searching spots",
			userId:1
		}),
		contentType : 'application/json',
		success : function(data) {
			alert(data);
		}
		
	});
}


function showfeedback() {
	$.ajax({
		type : "POST",
		url : "/kikspot/mobile/showfeedback.do",
		data : JSON.stringify({
			userId:1
		}),
		contentType : 'application/json',
		success : function(data) {
		alert(data);
		}
		
	});
}

function showfTotalfeedback(){

	$.ajax({
		type : "POST",
		url :  "/kikspot/mobile/totalfeedbacks.do",
		contentType : 'application/json',
		success : function(data) {
		alert(data);
		}

	   
	});
}
		
function deleteFeedback() {
	$.ajax({
		type : "POST",
		url : "/kikspot/mobile/deletefeedback.do",
		data : JSON.stringify({
			userId:1
		}),
		contentType : 'application/json',
		success : function(data) {
		alert(data);
		}
		
	});
}	
