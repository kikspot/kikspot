package com.kikspot.backend.user.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 * @author KNS-ACCONTS
 * 
 *  Created by Jeevan on November 17, 2015
 *  Model for UserPreferenceOptions..
 *  
 *
 */

@Entity
@Table(name="user_preference_options")
public class UserPreferenceOptions implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name="user_preference_options_id")
	private Integer userPreferenceOptionsId;
	
	@ManyToOne
	@JoinColumn(name="user_preference_id")
	private UserPreferences userPreferences;
	
	@Column(name="option")
	private String option;

	
	
	public Integer getUserPreferenceOptionsId() {
		return userPreferenceOptionsId;
	}

	public void setUserPreferenceOptionsId(Integer userPreferenceOptionsId) {
		this.userPreferenceOptionsId = userPreferenceOptionsId;
	}

	public UserPreferences getUserPreferences() {
		return userPreferences;
	}

	public void setUserPreferences(UserPreferences userPreferences) {
		this.userPreferences = userPreferences;
	}

	public String getOption() {
		return option;
	}

	public void setOption(String option) {
		this.option = option;
	}
	
	
	
	
	
	
	
	
}
