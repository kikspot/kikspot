package com.kikspot.backend.user.model;

public enum UserType {
	
	UNKNOWN(1),LOGGEDIN_USER(1),SOCIAL_USER(2),BYPASS_USER(3);
	
	
	private int value;
	
	private UserType(int value){
		this.value=value;
	}
	
	public int getUserType(){
		return value;
	}
	
	
	
}
