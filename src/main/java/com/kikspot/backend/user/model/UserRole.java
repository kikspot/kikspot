package com.kikspot.backend.user.model;

/**
 * 
 * @author KNS-ACCONTS
 * 
 *  Created by Jeevan on November 17, 2015
 *  
 *  Enum for USerRoles..
 *  
 *  Since UserRoles are Fixed. Saving them in ENUM, instead of getting them from seperate Table.
 *
 */
public enum UserRole {

	UNKNOWN(0),ROLE_ADMIN(1),ROLE_USER(2);
	
	
	private int value;
	
	private UserRole(int value){
		this.value=value; 
	}
	
	public int getUserRole(){
		return value;
	}
	
	
	/* public static UserRole parse(int id) {
		 UserRole userRole = null; // Default
         for (UserRole item : UserRole.values()) {
             if (item.getUserRole()==id) {
            	 userRole = item;
                 break;
             }
         }
         return userRole;
     }
	*/
	
	

};
