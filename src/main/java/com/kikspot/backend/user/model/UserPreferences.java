package com.kikspot.backend.user.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author KNS-ACCONTS
 * 
 *  Created by Jeevan on Novemeber 17, 2015
 *  Model for UserPreferences..
 *
 */


@Entity
@Table(name="user_preferences")
public class UserPreferences implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="preference_id")
	private Integer preferenceId;
	
	
	@Column(name="preference_item")
	private String preferenceItem;


	public Integer getPreferenceId() {
		return preferenceId;
	}


	public void setPreferenceId(Integer preferenceId) {
		this.preferenceId = preferenceId;
	}


	public String getPreferenceItem() {
		return preferenceItem;
	}


	public void setPreferenceItem(String preferenceItem) {
		this.preferenceItem = preferenceItem;
	}
		
	
	
	
	
	
}
