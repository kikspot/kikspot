package com.kikspot.backend.user.dao;

import com.kikspot.backend.exceptions.UserProbableReferralNotFoundException;
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.backend.user.model.UserProbableReferral;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.common.exception.UserNotSavedOrUpdatedException;

/**
 * Created By Bhagya On October 13th,2015
 *	Interface for UserDao, consists of abstract methods
 */

public interface UserDao{
	public KikspotUser getKikspotUserByEmail(String email) throws UserNotFoundException;
	public Integer saveOrUpdateKikspotUser(KikspotUser kikspotUser) throws UserNotSavedOrUpdatedException;
	public KikspotUser getKikspotUserforAuthentication(String username)throws UserNotFoundException;
	public KikspotUser getKikspotUserByPasswordResetToken(String token) throws UserNotFoundException;
	public KikspotUser getKikspotUserById(Integer id) throws UserNotFoundException;
	public KikspotUser getKikspotUserByUsername(String username)throws UserNotFoundException;
	public UserProbableReferral getUserProbableReferralByEmail(String email)throws UserProbableReferralNotFoundException;
	
}