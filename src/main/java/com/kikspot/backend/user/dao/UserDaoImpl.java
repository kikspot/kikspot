package com.kikspot.backend.user.dao;
/**
 * Created By Bhagya on october 13th,2015
 * Implementation class for UserDao interface
 */
import java.util.ArrayList;


import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import org.apache.log4j.Logger;

import com.kikspot.backend.exceptions.UserProbableReferralNotFoundException;
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.backend.user.model.UserProbableReferral;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.common.exception.UserNotSavedOrUpdatedException;

@Repository("userDao")
@Transactional
public class UserDaoImpl implements UserDao{
	
	private static Logger log=Logger.getLogger(UserDaoImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	
	/**
	 * Created By Bhagya On October 19th,2015
	 * @param email
	 * @return
	 * @throws UserNotFoundException
	 * 
	 * Method for getting the kikspotUser By Email
	 */
	@SuppressWarnings("unchecked")
	public KikspotUser getKikspotUserByEmail(String email) throws UserNotFoundException{
		log.info("inside getKikspotUserByEmail()");		
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(KikspotUser.class);
		criteria.add(Restrictions.eq("emailId", email));
		ArrayList<KikspotUser> users = (ArrayList<KikspotUser>) criteria.list();
		if(!users.isEmpty()){
			return users.get(0);
		}
		else{
			throw new UserNotFoundException();
		}
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Created by Jeevan on November 23, 2015
	 * 
	 *  Method to get KikspotUserByUsername...
	 *  
	 * @param username
	 * @return
	 * @throws UserNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public KikspotUser getKikspotUserByUsername(String username)throws UserNotFoundException{
		log.info("inside getKikspotUserByUsername() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(KikspotUser.class);
		criteria.add(Restrictions.eq("username", username));
		ArrayList<KikspotUser> users = (ArrayList<KikspotUser>) criteria.list();
		if(!users.isEmpty()){
			return users.get(0);
		}
		else{
			throw new UserNotFoundException();
		}
	}
	
	
	
	/**
	 * 
	 * @param username
	 * @return
	 * @throws UserNotFoundException
	 * 
	 *  Created by Jeevan on November 24, 2015
	 *  Method to  get User by Username or emaill.
	 *  
	 */ 
	@SuppressWarnings("unchecked")
	public KikspotUser getKikspotUserforAuthentication(String username)throws UserNotFoundException{
		log.info("inside getKikspotUserByUsername() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(KikspotUser.class);
		criteria.add(Restrictions.or(Restrictions.eqOrIsNull("emailId", username),Restrictions.eq("username", username)));
		ArrayList<KikspotUser> users = (ArrayList<KikspotUser>) criteria.list();
		if(!users.isEmpty()){
			return users.get(0);
		}
		else{
			throw new UserNotFoundException();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Created By Bhagya On October 19th,2015
	 * @param kikspotUser
	 * @return
	 * @throws UserNotSavedOrUpdatedException
	 * 
	 * Method for save or Update the kikspot User
	 */
	public Integer saveOrUpdateKikspotUser(KikspotUser kikspotUser) throws UserNotSavedOrUpdatedException{
		log.info("inside saveOrUpdateKikspotUser()");
		sessionFactory.getCurrentSession().saveOrUpdate(kikspotUser);
		sessionFactory.getCurrentSession().flush();
		return kikspotUser.getUserId();
	}
	
	
	
	
	/**
	 * Returns the user's  one who has given Password reset token
	 * password reset token sent to the user
	 * 
	 * @param token
	 * @throws UserNotFoundException 
	 * @throws Exception
	 *             if there is no user with given token OR any kind of exception
	 *             while interacting with db
	 */
	@SuppressWarnings("unchecked")
	public KikspotUser getKikspotUserByPasswordResetToken(String token) throws UserNotFoundException {
		log.info("inside getKikspotUserByPasswordResetToken()");
		
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(KikspotUser.class);
		criteria.add(Restrictions.eq("passwordToken", token));
		ArrayList<KikspotUser> users=(ArrayList<KikspotUser>) criteria.list();
		if (!users.isEmpty()) {
			return users.get(0);
		} else {
			throw new UserNotFoundException();
		}
	}
	
	
	
	
	/**
	 * Created By Bhagya On October 20th,2015
	 * @param id
	 * @return
	 * @throws UserNotFoundException
	 * 
	 * Method for getting kikspot user by Id
	 */
	@SuppressWarnings("unchecked")
	public KikspotUser getKikspotUserById(Integer id) throws UserNotFoundException{
		log.info("inside getKikspotUserById()");
		
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(KikspotUser.class);
		criteria.add(Restrictions.eq("userId", id));
		ArrayList<KikspotUser> users = (ArrayList<KikspotUser>) criteria.list();
		if(!users.isEmpty()){
			return users.get(0);
		}
		else{
			throw new UserNotFoundException();
		}
	}
	
	
	
	/**
	 * Created by Jeevan on Novemeber 23, 2015..
	 * 
	 * Method to getUserProbableReferralByEmail..
	 * 
	 * @param email
	 * @return
	 * @throws UserProbableReferralNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public UserProbableReferral getUserProbableReferralByEmail(String email)throws UserProbableReferralNotFoundException{
		log.info("inside getUserProbableReferralByEmail() ");
		ArrayList<UserProbableReferral> userProbableReferrals=new ArrayList<UserProbableReferral>();
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserProbableReferral.class);
		criteria.add(Restrictions.eq("email", email));
		userProbableReferrals=(ArrayList<UserProbableReferral>) criteria.list();
		if(userProbableReferrals.size()>0){
			return userProbableReferrals.get(0);
		}
		else{
			throw new UserProbableReferralNotFoundException();
		}
				
	}
	
	
	
	
}