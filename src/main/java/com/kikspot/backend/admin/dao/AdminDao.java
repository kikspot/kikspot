package com.kikspot.backend.admin.dao;

import java.util.ArrayList;

import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.frontend.common.exception.UserNotFoundException;

/**
 * Created By Bhagya On December 03rd,2015
 *	Interface For Admin Dao
 */
public interface AdminDao{
	public ArrayList<KikspotUser> getAllUsers() throws UserNotFoundException;
}