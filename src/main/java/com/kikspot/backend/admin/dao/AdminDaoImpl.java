package com.kikspot.backend.admin.dao;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.user.dto.KikspotUserDto;

/**
 * Created By Bhagya On december 03rd,2015
 *	Dao Implementation class for Admin Dao
 */
@Repository("adminDao")
@Transactional
public class AdminDaoImpl implements AdminDao{
	
	private static Logger log=Logger.getLogger(AdminDaoImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	/**
	 * Created By Bhagya On december 03rd,2015
	 * @return
	 * @throws UserNotFoundException
	 * Method for getting the list of all users from DB
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<KikspotUser> getAllUsers() throws UserNotFoundException{
		log.info("inside AdminDaoImpl -> getAllUsers()");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(KikspotUser.class);
		ArrayList<KikspotUser> users = (ArrayList<KikspotUser>) criteria.list();
		if(!users.isEmpty()){
			return users;
		}
		else{
			throw new UserNotFoundException();
		}
	}
	
}