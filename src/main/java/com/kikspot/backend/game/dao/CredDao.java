package com.kikspot.backend.game.dao;

import java.util.ArrayList;

import com.kikspot.backend.exceptions.CredRuleNotFoundException;
import com.kikspot.backend.exceptions.UserCredHistoryNotFoundException;
import com.kikspot.backend.exceptions.UserGameCredNotFoundException;
import com.kikspot.backend.game.model.CredRules;
import com.kikspot.backend.game.model.CredType;
import com.kikspot.backend.game.model.UserCredHistory;
import com.kikspot.backend.game.model.UserGameCreds;
import com.kikspot.backend.user.model.KikspotUser;

public interface CredDao {
	
	public Integer saveorUpdateCredRules(CredRules credRule)throws Exception;
	public CredRules getCredRulesByCredType(CredType credType)throws CredRuleNotFoundException;
	public Integer saveorUpdateUserCredHistory(UserCredHistory userCredHistory)throws Exception;
	public UserGameCreds getUserGameCredsByUser(KikspotUser kikspotUser)throws UserGameCredNotFoundException;
	public Integer saveorUpdateUserGameCred(UserGameCreds userGameCreds)throws Exception;
	public UserGameCreds getUserGameCredsOfUser(KikspotUser kikspotUser)throws UserGameCredNotFoundException;
	public ArrayList<UserGameCreds> getTopFiveUserGameCreds()throws UserGameCredNotFoundException;
	public UserCredHistory getMostRecentUserCredHistoryofUser(KikspotUser kikspotUser)throws UserCredHistoryNotFoundException;
	

}
