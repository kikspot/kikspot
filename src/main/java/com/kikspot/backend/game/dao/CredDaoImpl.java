package com.kikspot.backend.game.dao;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.kikspot.backend.exceptions.CredRuleNotFoundException;
import com.kikspot.backend.exceptions.UserCredHistoryNotFoundException;
import com.kikspot.backend.exceptions.UserGameCredNotFoundException;
import com.kikspot.backend.game.model.CredRules;
import com.kikspot.backend.game.model.CredType;
import com.kikspot.backend.game.model.UserCredHistory;
import com.kikspot.backend.game.model.UserGameCreds;
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.backend.user.model.UserType;



@Transactional
@Repository("credDao")
public class CredDaoImpl implements CredDao{
	
	private static Logger log=Logger.getLogger(CredDaoImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	
	/**
	 * 
	 *  Created by Jeevan on November 25, 2015.
	 *  Method to save or update Cred Rules..
	 * 
	 * 
	 * @param credRule
	 * @return
	 * @throws Exception
	 */
	public Integer saveorUpdateCredRules(CredRules credRule)throws Exception{
		log.info("inside saveOrUpdateCredRules() ");
		sessionFactory.getCurrentSession().saveOrUpdate(credRule);
		sessionFactory.getCurrentSession().flush();
		return credRule.getCredRuleId();
	}
	
	
	/**
	 * 
	 * @param credType
	 * @return
	 * @throws CredRuleNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public CredRules getCredRulesByCredType(CredType credType)throws CredRuleNotFoundException{
		log.info("inside getCredRulesByCredType() ");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(CredRules.class);
		criteria.add(Restrictions.eq("credType", credType));
		
		ArrayList<CredRules> credRules=(ArrayList<CredRules>) criteria.list();
		if(!credRules.isEmpty()){
			return credRules.get(0);
		}
		else{
			throw new CredRuleNotFoundException();
		}
	}
	
	
	/**
	 * 
	 * @param userCredHistoy
	 * @return
	 * @throws Exception
	 */
	public Integer saveorUpdateUserCredHistory(UserCredHistory userCredHistory)throws Exception{
		log.info("inside saveorUpdateUserCredHistory() ");
		sessionFactory.getCurrentSession().saveOrUpdate(userCredHistory);
		sessionFactory.getCurrentSession().flush();
		return userCredHistory.getUserCredHistoryId();
	}
	
	
	/**
	 * ed by Jeevan on November 27, 2015
	 * Method to getUserGameCredsByUser..
	 * 
	 * Creat
	 * 
	 * @param kikspotUser
	 * @return
	 * @throws UserGameCredNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public UserGameCreds getUserGameCredsByUser(KikspotUser kikspotUser)throws UserGameCredNotFoundException{
		log.info("inside getUserGameCredsByUser() ");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserGameCreds.class);
		criteria.add(Restrictions.eq("kikspotUser", kikspotUser));
		ArrayList<UserGameCreds> userGameCreds=(ArrayList<UserGameCreds>) criteria.list();
		if(!userGameCreds.isEmpty()){
			return userGameCreds.get(0);
		}
		else{
			throw new UserGameCredNotFoundException();
		}		
	}
	
	
	
	/**
	 * 
	 * @param userGameCreds
	 * @return
	 * @throws Exception
	 */
	public Integer saveorUpdateUserGameCred(UserGameCreds userGameCreds)throws Exception{
		log.info("inside saveorUpdateUserGameCred() ");
		sessionFactory.getCurrentSession().saveOrUpdate(userGameCreds);
		sessionFactory.getCurrentSession().flush();
		return userGameCreds.getUserGameId();
	}
	


	
	/**
	 * 
	 * @param kikspotUser
	 * @return
	 * @throws UserGameCredNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public UserGameCreds getUserGameCredsOfUser(KikspotUser kikspotUser)throws UserGameCredNotFoundException{
		log.info("inside getUserGameCredsOfUser() ");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserGameCreds.class)
						.add(Restrictions.eq("kikspotUser", kikspotUser));
		ArrayList<UserGameCreds> userGameCreds=(ArrayList<UserGameCreds>) criteria.list();
		if(!userGameCreds.isEmpty()){
			return userGameCreds.get(0);
		}
		else{
			throw new UserGameCredNotFoundException();
		}		
	}
	
	
	
	/**
	 * 
	 * @return
	 * @throws UserGameCredNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserGameCreds> getTopFiveUserGameCreds()throws UserGameCredNotFoundException{
		log.info("inside getTopFiveUserGameCreds() ");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserGameCreds.class);
		criteria.createCriteria("kikspotUser").add(Restrictions.ne("userType",UserType.BYPASS_USER));
		criteria.addOrder(Order.desc("creds"));
		criteria.setMaxResults(5);
		ArrayList<UserGameCreds> userGameCreds=(ArrayList<UserGameCreds>) criteria.list();
		if(!userGameCreds.isEmpty()){
			return userGameCreds;
		}
		else{
			throw new UserGameCredNotFoundException();
		}		
	}
	
	
	
	
	
	/**
	 * 
	 * @param kikspotUser
	 * @return
	 * @throws UserCredHistoryNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public UserCredHistory getMostRecentUserCredHistoryofUser(KikspotUser kikspotUser)throws UserCredHistoryNotFoundException{
		log.info("inside getMostRecentUserCredHistoryofUser() ");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UserCredHistory.class)
				.add(Restrictions.eq("kikspotUser", kikspotUser));
		criteria.addOrder(Order.desc("userCredHistoryId"));
		ArrayList<UserCredHistory> userCredHistories=(ArrayList<UserCredHistory>) criteria.list();
		if(!userCredHistories.isEmpty()){
			if(userCredHistories.size()>1){
				return userCredHistories.get(userCredHistories.size()-2);
			}
			return userCredHistories.get(0);
		}
		else{
			throw new UserCredHistoryNotFoundException();
		}
	}
	
	
	
}
