package com.kikspot.backend.game.dao;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.kikspot.backend.exceptions.GameLevelNotFoundException;
import com.kikspot.backend.exceptions.GameRuleNotFoundException;
import com.kikspot.backend.game.model.GameLevels;
import com.kikspot.backend.game.model.GameRules;



/**
 * Created by Jeevan on November 27, 2015
 * 
 *  Dao for GameDao
 *  
 * @author KNS-ACCONTS
 *
 */
@Repository("gameDao")
@Transactional
public class GameDaoImpl implements GameDao{

	
	private static Logger log=Logger.getLogger(GameDaoImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
/***************************************** GAME RULES ******************************************/	
	/**
	 * 
	 * @param gameRule
	 * @return
	 * @throws Exception
	 */
	public Integer saveorUpdateGameRule(GameRules gameRule)throws Exception{
		log.info("inside saveorUpdateGameRule() ");
		sessionFactory.getCurrentSession().saveOrUpdate(gameRule);
		sessionFactory.getCurrentSession().flush();
		return gameRule.getGameRuleId();
	}
	
	
	
	
	/**
	 * 
	 * @param gameId
	 * @return
	 * @throws GameRuleNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<GameRules> getGameRulesofGame(Integer gameId)throws GameRuleNotFoundException{
		log.info("inside getGameRulesofGame() ");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(GameRules.class);
		criteria.add(Restrictions.eq("game.gameId", gameId));
		ArrayList<GameRules> gameRules=(ArrayList<GameRules>) criteria.list();
		if(!gameRules.isEmpty()){
			return gameRules;
		}
		else{
			throw new GameRuleNotFoundException();
		}		
	}
	
	

	
	
	
	
/********************************* GAME LEVELS **********************************************************/	
	
	/**
	 * 
	 * @param gameLevel
	 * @return
	 * @throws Exception
	 */
	public Integer saveorUpdateGameLevel(GameLevels gameLevel)throws Exception{
		log.info("inside saveorUpdateGameLevel() ");
		sessionFactory.getCurrentSession().saveOrUpdate(gameLevel);
		sessionFactory.getCurrentSession().flush();
		return gameLevel.getGameLevelId();
	}
	
	
	
	
	
	/**
	 * 
	 * Created by Jeevan on DEC 03, 2015
	 * 
	 * Method to get GameLevelsofGame..
	 * 
	 * 
	 * @param gameId
	 * @return
	 * @throws GameLevelNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<GameLevels> getGameLevelsofGame(Integer gameId)throws GameLevelNotFoundException{
		log.info("inside getGameLevelsofGame()");
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(GameLevels.class);
		criteria.add(Restrictions.eq("game.gameId", gameId));
		ArrayList<GameLevels> gameLevels=(ArrayList<GameLevels>) criteria.list();
		if(!gameLevels.isEmpty()){
			return gameLevels;
		}
		else{
			throw new GameLevelNotFoundException();
		}
	}
	
	
	
	
	
	
	
		
	
}
