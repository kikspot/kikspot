package com.kikspot.backend.game.dao;

import java.util.ArrayList;

import com.kikspot.backend.exceptions.GameRuleNotFoundException;
import com.kikspot.backend.game.model.GameRules;


public interface GameDao {
	
	public Integer saveorUpdateGameRule(GameRules gameRule)throws Exception;
	public ArrayList<GameRules> getGameRulesofGame(Integer gameId)throws GameRuleNotFoundException;
	

}
