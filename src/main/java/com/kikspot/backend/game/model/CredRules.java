package com.kikspot.backend.game.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 
 *  Created by Jeevan on November 26, 2015
 *   Model for CredRules.. 
 *   
 *   It will store all Cred Rules is Database
 *   
 * @author KNS-ACCONTS
 *
 */


@Entity
@Table(name="cred_rules")
public class CredRules implements Serializable{
	
	@Id
	@GeneratedValue
	@Column(name="cred_rule_id")
	private Integer credRuleId; 
	
	@Enumerated(EnumType.STRING)
	@Column(name="cred_type")
	private CredType credType;
	
	@ManyToOne
	@JoinColumn(name="game_rule")
	private GameRules gameRule;
	
	@Column(name="creds")
	private Integer creds;
	
	@Transient
	private Integer totalCredRules;
	
	
	

	public Integer getCredRuleId() {
		return credRuleId;
	}

	public void setCredRuleId(Integer credRuleId) {
		this.credRuleId = credRuleId;
	}

	public CredType getCredType() {
		return credType;
	}

	public void setCredType(CredType credType) {
		this.credType = credType;
	}

	public GameRules getGameRule() {
		return gameRule;
	}

	public void setGameRule(GameRules gameRule) {
		this.gameRule = gameRule;
	}

	public Integer getCreds() {
		return creds;
	}

	public void setCreds(Integer creds) {
		this.creds = creds;
	}

	public Integer getTotalCredRules() {
		return totalCredRules;
	}

	public void setTotalCredRules(Integer totalCredRules) {
		this.totalCredRules = totalCredRules;
	}
	
	
	

}
