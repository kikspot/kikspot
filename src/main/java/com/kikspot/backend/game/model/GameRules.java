package com.kikspot.backend.game.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


/**
 * 
 * @author KNS-ACCONTS
 *
 * 
 *  Created by Jeevan on November 20, 2015
 *  
 *  Model for Game Rules...
 *  
 *
 */
@Entity
@Table(name="game_rules")
public class GameRules implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue
	@Column(name="game_rule_id")
	private Integer gameRuleId;
	
	
	@ManyToOne
	@JoinColumn(name="game_id")
	private Game game;
	
	
	@Column(name="rule")
	private String rule;
	
	
	@Column(name="rule_limit")
	private Integer ruleLimit;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="expiry_date")
	private Date expiryDate;
	
	
	@Column(name="creds")
	private Integer creds;

	@Transient
	private Integer totalRules;
	

	public Integer getGameRuleId() {
		return gameRuleId;
	}


	public void setGameRuleId(Integer gameRuleId) {
		this.gameRuleId = gameRuleId;
	}


	public Game getGame() {
		return game;
	}


	public void setGame(Game game) {
		this.game = game;
	}


	public String getRule() {
		return rule;
	}


	public void setRule(String rule) {
		this.rule = rule;
	}


	public Integer getRuleLimit() {
		return ruleLimit;
	}


	public void setRuleLimit(Integer ruleLimit) {
		this.ruleLimit = ruleLimit;
	}


	public Date getExpiryDate() {
		return expiryDate;
	}


	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}


	public Integer getCreds() {
		return creds;
	}


	public void setCreds(Integer creds) {
		this.creds = creds;
	}


	public Integer getTotalRules() {
		return totalRules;
	}


	public void setTotalRules(Integer totalRules) {
		this.totalRules = totalRules;
	}
	
	
	
	
	
	
}
