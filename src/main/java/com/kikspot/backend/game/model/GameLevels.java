package com.kikspot.backend.game.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * 
 * Created by Jeevan on Novemeber 20, 2015
 * 
 *  Model for Game Levels..
 *  All Game Level related information will be configured here...
 * 
 * @author KNS-ACCONTS
 *
 */
@Entity
@Table(name="game_levels")
public class GameLevels implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="game_level_id")
	private Integer gameLevelId;
	
	@ManyToOne
	@JoinColumn(name="game_id")
	private Game game;
	
	@Column(name="level")
	private String level;
	
	
	@Column(name="min_creds")
	private Integer minCreds;


	
	
	public Integer getGameLevelId() {
		return gameLevelId;
	}


	public void setGameLevelId(Integer gameLevelId) {
		this.gameLevelId = gameLevelId;
	}


	public Game getGame() {
		return game;
	}


	public void setGame(Game game) {
		this.game = game;
	}


	public String getLevel() {
		return level;
	}


	public void setLevel(String level) {
		this.level = level;
	}


	public Integer getMinCreds() {
		return minCreds;
	}


	public void setMinCreds(Integer minCreds) {
		this.minCreds = minCreds;
	}
	
	
	
	
	
	
	
	
	
	
	
	

}
