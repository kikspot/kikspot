package com.kikspot.backend.exceptions;

public class CredRuleNotFoundException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	String msg;
	
	public CredRuleNotFoundException() {
		msg="Cred Rule Not Found";
	}
	
	
	CredRuleNotFoundException(String msg){
		this.msg=msg;
	}
	
	
	@Override
	public String toString() {		
		return this.msg;
	}

}
