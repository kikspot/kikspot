package com.kikspot.backend.exceptions;

public class GameRuleNotFoundException extends Exception{

	private String message;
	
	public GameRuleNotFoundException() {
		message="No Game Rules Found";
	}
	
	
	public GameRuleNotFoundException(String message){
		this.message=message;
	}
	
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return message;
	}
	
}
