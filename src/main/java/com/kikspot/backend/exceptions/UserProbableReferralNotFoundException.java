package com.kikspot.backend.exceptions;

/**
 * 
 * @author KNS-ACCONTS
 * 
 *  Created by Jeevan on Novemeber 23, 2015
 *  
 *   Exception class.
 *
 */

public class UserProbableReferralNotFoundException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Override
	public String toString() {	
		return "User Probable Referral Not Found";
	}

}
