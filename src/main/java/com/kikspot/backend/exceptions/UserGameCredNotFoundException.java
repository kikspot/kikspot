package com.kikspot.backend.exceptions;

public class UserGameCredNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String message;
	
	public UserGameCredNotFoundException() {
		message="User Game Cred Not Found";
	}
	
	
	public UserGameCredNotFoundException(String message){
		this.message=message;
	}
	
	
	@Override
	public String toString() {
		
		return message;
	}
}
