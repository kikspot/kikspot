package com.kikspot.backend.exceptions;
/**
 * Created By Bhagya On November 30th,2015
 *	User defined Exception for handling the , when business information not available for requested business at yelp API
 */
public class BusinessInformationUnavailableException extends Exception{
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "Business Information Not Available for requested business at Yelp API";
	}
}