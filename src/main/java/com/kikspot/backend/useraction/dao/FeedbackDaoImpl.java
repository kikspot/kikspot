package com.kikspot.backend.useraction.dao;

/**
 * 
 * 
 * Created by Firdous on 23-11-2015
 * Dao to handle the  feedbacks of users
 * 
 * 
 */
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kikspot.backend.exceptions.FeedBackNotFoundException;
import com.kikspot.backend.useraction.model.Feedback;

@Repository("feedbackDao")
@Transactional
public class FeedbackDaoImpl implements FeedbackDao {


private static Logger log=Logger.getLogger(FeedbackDaoImpl.class);
	


	@Autowired
	private SessionFactory sessionFactory;
	
	/**
	 * 
	 * method to save the feedback  of a user inside the database
	 * 
	 */
	@Override
	public Integer saveFeedBack(Feedback feedback) throws Exception {		
		log.info("inside save feedback()");		
		sessionFactory.getCurrentSession().saveOrUpdate(feedback);			
		this.sessionFactory.getCurrentSession().flush();		
		return feedback.getFeedbackId();	
	}

	
	
	/**
	 * 
	 * method to get the feedback  of a user with given user id
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Feedback getFeedback(Integer user_id) throws FeedBackNotFoundException {		
		ArrayList<Feedback> users = new ArrayList<Feedback>();
		users = (ArrayList<Feedback>) sessionFactory.getCurrentSession()
			.createQuery("from Feedback where user_id=?")
			.setParameter(0,user_id)
			.list();
		if (users.size() > 0) {
			return users.get(0);
		}
		else {
			throw new FeedBackNotFoundException();
		}
	}
	
	
	/**
	 * 
	 * 
	 * method to get the total number of feedbacks send by users
	 * 
	 * 
	 */
	
	public Integer getTotalFeedbacks() throws Exception{		
		log.info("inside getTotalFeedbacks()");		
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(Feedback.class);		
		criteria.setProjection(Projections.count("feedbackId")).list();		
		Integer totalFeedbacks=Integer.valueOf (criteria.list().get(0).toString());		
		return totalFeedbacks;
	}
	
	

    /**
     * 
     * created by Firdous on 26-11-2015
     * method to delete feedback of a user
     * 
     * 
     */

	@Override
	public void deleteFeedback(Feedback feedback) throws Exception {		
		sessionFactory.getCurrentSession().delete(feedback);		
		sessionFactory.getCurrentSession().flush();		
	}
	


}
