package com.kikspot.backend.useraction.dao;

import com.kikspot.backend.useraction.model.Feedback;

public interface FeedbackDao {
	
	 public Integer saveFeedBack(Feedback feedback) throws Exception;

     public Feedback getFeedback(Integer user_id) throws Exception;

     public Integer getTotalFeedbacks() throws Exception;
     
     public void deleteFeedback(Feedback feedback) throws Exception;

}
