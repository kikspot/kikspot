package com.kikspot.backend.useraction.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.kikspot.backend.user.model.KikspotUser;

/**
 * 
 * 
 * 
 * @author KNS-ACCONTS
 *
 *
 *  Created by Jeevan on November 23, 2015
 *  
 *   Model for Feedback..
 *   
 *
 *
 */
@Entity
@Table(name="feedback")
public class Feedback  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name="feedback_id")
	private Integer feedbackId;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private KikspotUser kikspotUser;
	
	@Column(name="feedback_message")
	private String feedbackMessage;
	
	@Column(name="feedback_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date feedbackDate;

	public Integer getFeedbackId() {
		return feedbackId;
	}

	public void setFeedbackId(Integer feedbackId) {
		this.feedbackId = feedbackId;
	}

	public KikspotUser getKikspotUser() {
		return kikspotUser;
	}

	public void setKikspotUser(KikspotUser kikspotUser) {
		this.kikspotUser = kikspotUser;
	}

	public String getFeedbackMessage() {
		return feedbackMessage;
	}

	public void setFeedbackMessage(String feedbackMessage) {
		this.feedbackMessage = feedbackMessage;
	}

	public Date getFeedbackDate() {
		return feedbackDate;
	}

	public void setFeedbackDate(Date feedbackDate) {
		this.feedbackDate = feedbackDate;
	}
	
	
	
	
}
