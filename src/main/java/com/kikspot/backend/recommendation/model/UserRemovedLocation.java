package com.kikspot.backend.recommendation.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.kikspot.backend.user.model.KikspotUser;


/**
 * 
 * @author KNS-ACCONTS
 *
 * 
 *  Created by Jeevan on Novemeber 20, 2015
 *  
 *   Model for UserRemovedLocation...
 *   
 *   All Locations Which User Does not want to view will be persisted in UserRemovedLocation.
 *
 */

@Entity
@Table(name="user_removed_location")
public class UserRemovedLocation  implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="removed_location_id")
	private Integer removedLocationId;
	
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private KikspotUser kikspotUser;
	
	@ManyToOne
	@JoinColumn(name="location_id")
	private RecommendationLocation recommendationLocation;
	
	@Column(name="location_API_ID")
	private String locationAPIId;
	

	public Integer getRemovedLocationId() {
		return removedLocationId;
	}

	public void setRemovedLocationId(Integer removedLocationId) {
		this.removedLocationId = removedLocationId;
	}

	public KikspotUser getKikspotUser() {
		return kikspotUser;
	}

	public void setKikspotUser(KikspotUser kikspotUser) {
		this.kikspotUser = kikspotUser;
	}

	public RecommendationLocation getRecommendationLocation() {
		return recommendationLocation;
	}

	public void setRecommendationLocation(
			RecommendationLocation recommendationLocation) {
		this.recommendationLocation = recommendationLocation;
	}

	public String getLocationAPIId() {
		return locationAPIId;
	}

	public void setLocationAPIId(String locationAPIId) {
		this.locationAPIId = locationAPIId;
	}
	
	
	
	
	

}
