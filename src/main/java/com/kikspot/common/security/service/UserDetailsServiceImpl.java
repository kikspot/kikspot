package com.kikspot.common.security.service;
/**
 * Created By Bhagya On October 13th,2015
 * Implementation class for core interface of UserDeatailsService 
 * 		which loads user specific data
 */
import java.util.ArrayList;

import java.util.Collection;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.kikspot.frontend.user.dto.KikspotUserDto;
import com.kikspot.frontend.user.service.UserService;


@Service(value="userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService{
	@Resource(name="userService")
	private UserService userService;
	
	
	private static Logger log=Logger.getLogger(UserDetailsServiceImpl.class);
	@Override
	public UserDetails loadUserByUsername(String emailId)
			throws UsernameNotFoundException, DataAccessException {
		log.info("inside loadUserByUsername ");
		KikspotUserDto userDto=null;
		try{
			userDto=this.userService.getUserByEmailId(emailId);
		}
		catch(Exception e){
			log.error("inside user service "+e.toString());
		}
		
		if(userDto ==null){
			throw new UsernameNotFoundException("User Not Found");
		}
		String email=userDto.getEmailId();
		String password=userDto.getPassword();
		
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority(userDto.getUserRole()));
		Boolean enabled =true;
		Boolean accountNonExpired =true;
		Boolean credentialsNonExpired =true;
		Boolean accountNonLocked =true;
		User user=new User(email, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
	    return (UserDetails)user;
		 
	}

}