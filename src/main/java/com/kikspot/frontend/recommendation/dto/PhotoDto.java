package com.kikspot.frontend.recommendation.dto;
/**
 * Created By Bhagya On November 26th, 2015
 *	 Dto For Google Places API Photos
 */
public class PhotoDto{
	private  String reference;
    private  Integer width; 
    private Integer height;
    
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
    
    
}