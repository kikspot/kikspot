package com.kikspot.frontend.recommendation.dto;


/**
 * Created By Bhagya On November 26th,2015
 * Dto For the Reviews at Google Places API And Yelp API
 *
 */

public class ReviewsDto{
	    private String author; 
	    private String authorUrl;
	    private String profilePhotoURL;
	    private String language ; 
	    private String text;
	    private Integer rating;
	    private Long time;
	    
		public String getAuthor() {
			return author;
		}
		public void setAuthor(String author) {
			this.author = author;
		}
		public String getAuthorUrl() {
			return authorUrl;
		}
		public void setAuthorUrl(String authorUrl) {
			this.authorUrl = authorUrl;
		}
		public String getProfilePhotoURL() {
			return profilePhotoURL;
		}
		public void setProfilePhotoURL(String profilePhotoURL) {
			this.profilePhotoURL = profilePhotoURL;
		}
		public String getLanguage() {
			return language;
		}
		public void setLanguage(String language) {
			this.language = language;
		}
		public String getText() {
			return text;
		}
		public void setText(String text) {
			this.text = text;
		}
		public Integer getRating() {
			return rating;
		}
		public void setRating(Integer rating) {
			this.rating = rating;
		}
		public Long getTime() {
			return time;
		}
		public void setTime(Long time) {
			this.time = time;
		}
	    
		
	    
}