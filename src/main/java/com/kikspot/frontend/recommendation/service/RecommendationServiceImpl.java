package com.kikspot.frontend.recommendation.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kikspot.backend.exceptions.InvalidRequestException;
import com.kikspot.backend.exceptions.NoResultsFoundException;
import com.kikspot.backend.exceptions.OverQueryLimitException;
import com.kikspot.backend.exceptions.PlaceNotFoundException;
import com.kikspot.backend.exceptions.RequestDeniedException;
import com.kikspot.backend.exceptions.UnknownErrorException;
import com.kikspot.frontend.recommendation.dto.PlaceDto;
import com.kikspot.frontend.recommendation.dto.RecommendationLocationDto;
import com.kikspot.frontend.recommendation.helper.googlePlacesAPI.GooglePlacesAPIProvider;
import com.kikspot.frontend.recommendation.helper.yelpAPI.YelpAPIProvider;



/**
 * Created By Bhagya On November 23rd, 2015
 * Implementation class for Recommendation Services
 */
@Transactional
@Service("recommendationService")
public class RecommendationServiceImpl implements RecommendationService{
	
	private static Logger log=Logger.getLogger(RecommendationServiceImpl.class);
	
	@Resource(name="googlePlacesAPIProvider")
	private GooglePlacesAPIProvider googlePlacesAPIProvider;
	
	@Resource(name="yelpAPIProvider")
	private YelpAPIProvider yelpAPIProvider;
	
	
	
	
	/**
	 * Created By Bhagya On November 24th, 2015
	 * @param userId
	 * @param latitude
	 * @param longitude
	 * @return
	 * @throws Exception
	 * 
	 * Method for getting the near by location based on lat and lng from google places api
	 * Saving the recommendation locations to Db
	 * 
	 *  1. Remove user deleted Locations..
	 *  2. Get User Ratings
	 *  3. Get Trending from User.
	 *  4. Get Game Rules..
	 *  5. Handle Sorting..
	 *  
	 *  
	 * 
	 */
	public ArrayList<RecommendationLocationDto> getLocations(Integer userId,Double latitude,Double longitude) throws Exception{
		log.info("inside RecommendationServiceImpl->getLocationsAndSaveRecommendationLocations()");
		ArrayList<PlaceDto> placeDtos=this.googlePlacesAPIProvider.getNearByPlacesByLatAndLng(latitude, longitude);
		ArrayList<RecommendationLocationDto> recommendationLocationDtos=new ArrayList<RecommendationLocationDto>();
		for(PlaceDto placeDto:placeDtos){
			RecommendationLocationDto recommendationLocationDto=new RecommendationLocationDto();
			recommendationLocationDto.setLocationAPIId(placeDto.getPlaceId());
			recommendationLocationDto.setLocationName(placeDto.getPlaceName());
			recommendationLocationDto.setLatitude(placeDto.getLatitude());
			recommendationLocationDto.setLongitude(placeDto.getLongitude());
			recommendationLocationDto.setIconUrl(placeDto.getIconURL());
			recommendationLocationDto.setDistance(this.distance(latitude, longitude, recommendationLocationDto.getLatitude(), recommendationLocationDto.getLongitude()));			
			recommendationLocationDtos.add(recommendationLocationDto);
		}
		return  recommendationLocationDtos;
	}
	
	
	
	/**
	 * Created By Bhagya On November 25th,2015
	 * @param locationAPIId
	 * @return
	 * @throws Exception
	 * 
	 * Method For getting the details of lOcation By Location APIId from Google places API
	 */
	
	public RecommendationLocationDto getLocationDetailsByLocationAPIId(String locationAPIId) throws Exception{
		log.info("inside RecommendationServiceImpl->getLocationDetailsByLocationAPIId()");
		try{
			PlaceDto placeDto=this.googlePlacesAPIProvider.getPlaceDetailsByplaceId(locationAPIId);
			RecommendationLocationDto recommendationLocationDto=new RecommendationLocationDto();
			recommendationLocationDto.setLocationAPIId(placeDto.getPlaceId());
			recommendationLocationDto.setLocationName(placeDto.getPlaceName());
			recommendationLocationDto.setLatitude(placeDto.getLatitude());
			recommendationLocationDto.setLongitude(placeDto.getLongitude());
			recommendationLocationDto.setRatings(placeDto.getRating().toString());
			recommendationLocationDto.setIconUrl(placeDto.getIconURL());
			recommendationLocationDto.setImage(placeDto.getImageURL());
			recommendationLocationDto.setUrl(placeDto.getWebsiteURL());
			recommendationLocationDto.setPhoneNo(placeDto.getPhoneNumber());
			recommendationLocationDto.setAddress(placeDto.getAddress());
			recommendationLocationDto.setIsNew(false);
			return recommendationLocationDto;
		}
		catch(RequestDeniedException e){
			System.out.println(" Request was Denied because of lack of an invalid key parameter.");
			throw new Exception();
		}
		catch(InvalidRequestException e){
			System.out.println("Inavalid Request,generally indicates that the query (reference) is missing or place Details doesn't exist for requested Place Id");
			throw new Exception();
		}
		catch(NoResultsFoundException e){
			System.out.println("No Results Found At Requested Criteria");
			throw new NoResultsFoundException();
		}
		catch(OverQueryLimitException e){
			System.out.println("Exceeds the Limit For Accessing Google Places API");
			throw new Exception();
		}
		catch(PlaceNotFoundException e){
			System.out.println(" Place Not Found At Google Places API");
			throw new Exception();
		}
		catch(UnknownErrorException e){
			System.out.println("Indicates a server-side error,trying again may be successful ");
			throw new Exception();
		}
		catch(Exception e){
			throw new Exception();
		}	
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 *  Created by Jeevan on December 07, 2015
	 *  
	 *  Method to getDistance between 2 locations based on Geocoodinates.
	 *  
	 *  Return result in meters
	 * 
	 * @param lat1
	 * @param lon1
	 * @param lat2
	 * @param lon2
	 * @param unit
	 * @return
	 */
	 private double distance(double lat1, double lon1, double lat2, double lon2) {
		  double theta = lon1 - lon2;
		  double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
		  dist = Math.acos(dist);
		  dist = rad2deg(dist);
		  dist = dist * 60 * 1.1515;
		  //conversion of miles to KM and then to Meters
		   dist = (dist * 1.609344)*1000;		 
		 /* else if (unit == 'N') {
			  dist = dist * 0.8684;
		    }*/
		   BigDecimal bd=new BigDecimal(dist);
		   return  bd.setScale(2,RoundingMode.HALF_UP).doubleValue();
		}
	
	
	
		/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
		/*::  This function converts decimal degrees to radians             :*/
		/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
		private double deg2rad(double deg) {
		  return (deg * Math.PI / 180.0);
		}
		
		
		/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
		/*::  This function converts radians to decimal degrees             :*/
		/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
		private double rad2deg(double rad) {
		  return (rad * 180.0 / Math.PI);
		}
		
		
		
		
		
}
	
	
	
	
	
	
	
	
