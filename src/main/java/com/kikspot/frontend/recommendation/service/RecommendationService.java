package com.kikspot.frontend.recommendation.service;

import java.util.ArrayList;

import com.kikspot.frontend.recommendation.dto.RecommendationLocationDto;

/**
 * Created By Bhagya On November 23rd,2015
 * Interface for the Recommendation Service
 */
public interface RecommendationService{
	public ArrayList<RecommendationLocationDto> getLocations(Integer userId,Double latitude,Double longitude) throws Exception;
	public RecommendationLocationDto getLocationDetailsByLocationAPIId(String locationAPIId) throws Exception;
}