package com.kikspot.frontend.recommendation.helper.yelpAPI;

import java.util.ArrayList;

import com.kikspot.backend.exceptions.BusinessInformationUnavailableException;
import com.kikspot.frontend.recommendation.dto.PlaceDto;

import twitter4j.org.json.JSONException;


/**
 * 
 *Created By bhagya On November 23rd,2015
 * Accessing the Yelp API
 */
public interface YelpAPIProvider{
	 public ArrayList<PlaceDto> getNearByPlacesFromYelpAPI(Double latitude,Double longitude) throws Exception;
	 public PlaceDto getPlaceInformationByPlaceIdFromYelpAPI(String businessId) throws JSONException, BusinessInformationUnavailableException, Exception;
}