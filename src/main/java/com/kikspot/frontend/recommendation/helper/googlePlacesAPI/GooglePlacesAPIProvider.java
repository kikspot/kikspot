package com.kikspot.frontend.recommendation.helper.googlePlacesAPI;

import java.io.IOException


;
import java.util.ArrayList;

import com.kikspot.backend.exceptions.InvalidRequestException;
import com.kikspot.backend.exceptions.NoResultsFoundException;
import com.kikspot.backend.exceptions.OverQueryLimitException;
import com.kikspot.backend.exceptions.PlaceNotFoundException;
import com.kikspot.backend.exceptions.RequestDeniedException;
import com.kikspot.backend.exceptions.UnknownErrorException;
import com.kikspot.frontend.recommendation.dto.PlaceDto;

import twitter4j.org.json.JSONException;


/**
 * Created By Bhagya On November 26th,2015
 *	Interface For Google Places API Provider
 */
public interface GooglePlacesAPIProvider {
	public ArrayList<PlaceDto> getNearByPlacesByLatAndLng(Double latitude,Double longitude) throws Exception;
	public PlaceDto getPlaceDetailsByplaceId(String placeId) throws IOException, JSONException, RequestDeniedException, InvalidRequestException, NoResultsFoundException, PlaceNotFoundException, OverQueryLimitException, UnknownErrorException;
}