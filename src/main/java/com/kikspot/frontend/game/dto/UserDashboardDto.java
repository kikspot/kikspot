package com.kikspot.frontend.game.dto;

import java.util.ArrayList;

import com.kikspot.frontend.user.dto.KikspotUserDto;



public class UserDashboardDto {
	
	private KikspotUserDto kikspotUserDto;
	private ArrayList<GameRulesDto> gameRulesDtos=new ArrayList<GameRulesDto>();
	private ArrayList<UserGameCredsDto> userGameCredsDtos=new ArrayList<UserGameCredsDto>();
	private UserGameCredsDto userGameCredDto;
	private String trending;
	
	
	

	public KikspotUserDto getKikspotUserDto() {
		return kikspotUserDto;
	}
	public void setKikspotUserDto(KikspotUserDto kikspotUserDto) {
		this.kikspotUserDto = kikspotUserDto;
	}
	public ArrayList<GameRulesDto> getGameRulesDtos() {
		return gameRulesDtos;
	}
	public void setGameRulesDtos(ArrayList<GameRulesDto> gameRulesDtos) {
		this.gameRulesDtos = gameRulesDtos;
	}
	public ArrayList<UserGameCredsDto> getUserGameCredsDtos() {
		return userGameCredsDtos;
	}
	public void setUserGameCredsDtos(ArrayList<UserGameCredsDto> userGameCredsDtos) {
		this.userGameCredsDtos = userGameCredsDtos;
	}
	public UserGameCredsDto getUserGameCredDto() {
		return userGameCredDto;
	}
	public void setUserGameCredDto(UserGameCredsDto userGameCredDto) {
		this.userGameCredDto = userGameCredDto;
	}
	public String getTrending() {
		return trending;
	}
	public void setTrending(String trending) {
		this.trending = trending;
	}
	
	
	
	
}
