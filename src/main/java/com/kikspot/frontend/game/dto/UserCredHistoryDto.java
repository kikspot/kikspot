package com.kikspot.frontend.game.dto;

import java.util.Date;

import com.kikspot.backend.game.model.UserCredHistory;
import com.kikspot.frontend.user.dto.KikspotUserDto;


/**
 * Created by Jeevam on November 26, 2015
 * 
 * Dto to preserve all USer Cred Rating Change history..
 * 
 * @author KNS-ACCONTS
 *
 */
public class UserCredHistoryDto {
	
	private Integer userCredHistoryId;
	private KikspotUserDto kikspotUserDto;
	private Integer cred;
	private Date date;
	
	
	
	public Integer getUserCredHistoryId() {
		return userCredHistoryId;
	}
	public void setUserCredHistoryId(Integer userCredHistoryId) {
		this.userCredHistoryId = userCredHistoryId;
	}
	public KikspotUserDto getKikspotUserDto() {
		return kikspotUserDto;
	}
	public void setKikspotUserDto(KikspotUserDto kikspotUserDto) {
		this.kikspotUserDto = kikspotUserDto;
	}
	public Integer getCred() {
		return cred;
	}
	public void setCred(Integer cred) {
		this.cred = cred;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	
	/**
	 * 
	 * @param userCredHistory
	 * @return
	 */
	public static UserCredHistoryDto populateUserCredHistoryDto(UserCredHistory userCredHistory){
		UserCredHistoryDto userCredHistoryDto=new UserCredHistoryDto();
		userCredHistoryDto.setCred(userCredHistory.getCred());
		userCredHistoryDto.setUserCredHistoryId(userCredHistory.getUserCredHistoryId());
		userCredHistoryDto.setKikspotUserDto(KikspotUserDto.populateKikspotUserDto(userCredHistory.getKikspotUser()));
		userCredHistoryDto.setDate(userCredHistory.getDate());
		return userCredHistoryDto;
	}
	

}
