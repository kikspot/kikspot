package com.kikspot.frontend.game.dto;

import com.kikspot.backend.game.model.UserGameCreds;
import com.kikspot.frontend.user.dto.KikspotUserDto;


/**
 * DTO to maintain Creds of USer
 * along with Game Details..
 * 
 * 
 * @author KNS-ACCONTS
 *
 */
public class UserGameCredsDto {

	private Integer userGameId;
	private GameDto gameDto;
	private String level;
	private KikspotUserDto kikspotUserDto;
	private Integer creds;
	private String credType;
	
	
	public Integer getUserGameId() {
		return userGameId;
	}
	public void setUserGameId(Integer userGameId) {
		this.userGameId = userGameId;
	}
	public GameDto getGameDto() {
		return gameDto;
	}
	public void setGameDto(GameDto gameDto) {
		this.gameDto = gameDto;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public KikspotUserDto getKikspotUserDto() {
		return kikspotUserDto;
	}
	public void setKikspotUserDto(KikspotUserDto kikspotUserDto) {
		this.kikspotUserDto = kikspotUserDto;
	}
	public Integer getCreds() {
		return creds;
	}
	public void setCreds(Integer creds) {
		this.creds = creds;
	}
	public String getCredType() {
		return credType;
	}
	public void setCredType(String credType) {
		this.credType = credType;
	}
	
	
	public static UserGameCredsDto populateUserGameCredsDto(UserGameCreds userGameCreds){
		UserGameCredsDto userGameCredsDto=new UserGameCredsDto();
		userGameCredsDto.setUserGameId(userGameCreds.getUserGameId());
		userGameCredsDto.setLevel(userGameCreds.getLevel());
		userGameCredsDto.setCreds(userGameCreds.getCreds());
		userGameCredsDto.setCredType(userGameCreds.getCredType().name());
		userGameCredsDto.setKikspotUserDto(KikspotUserDto.populateKikspotUserDto(userGameCreds.getKikspotUser()));
		return userGameCredsDto;
	}
	
	
	
	
	
	
	
	
}
