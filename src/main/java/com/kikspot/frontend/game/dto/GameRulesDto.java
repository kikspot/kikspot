package com.kikspot.frontend.game.dto;

import java.util.Date;

import com.kikspot.backend.game.model.GameRules;


/**
 * Created by Jeevan on November 26, 2015
 * 
 * Dto for Game Rules..
 * 
 * @author KNS-ACCONTS
 *
 */
public class GameRulesDto {
	
	private Integer gameRuleId;
	
	private GameDto gameDto;
	
	private String rule;
	
	private Integer ruleLimit;
	
	private Integer creds;
	
	private Date expiryDate;
	
	private Integer totalRules;

	public Integer getGameRuleId() {
		return gameRuleId;
	}

	public void setGameRuleId(Integer gameRuleId) {
		this.gameRuleId = gameRuleId;
	}

	public GameDto getGameDto() {
		return gameDto;
	}

	public void setGameDto(GameDto gameDto) {
		this.gameDto = gameDto;
	}

	public String getRule() {
		return rule;
	}

	public void setRule(String rule) {
		this.rule = rule;
	}

	public Integer getRuleLimit() {
		return ruleLimit;
	}

	public void setRuleLimit(Integer ruleLimit) {
		this.ruleLimit = ruleLimit;
	}

	public Integer getCreds() {
		return creds;
	}

	public void setCreds(Integer creds) {
		this.creds = creds;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Integer getTotalRules() {
		return totalRules;
	}

	public void setTotalRules(Integer totalRules) {
		this.totalRules = totalRules;
	}
	
	
	public static GameRulesDto populateGameRulesDto(GameRules gameRule){
		GameRulesDto gameRuleDto=new GameRulesDto();
		gameRuleDto.setGameRuleId(gameRule.getGameRuleId());
		gameRuleDto.setCreds(gameRule.getCreds());
		if(null!=gameRule.getExpiryDate())
			gameRuleDto.setExpiryDate(gameRule.getExpiryDate());
		if(null!=gameRule.getGame())
			gameRuleDto.setGameDto(GameDto.populateGameDto(gameRule.getGame()));
		gameRuleDto.setRule(gameRule.getRule());
		if(null!=gameRule.getRuleLimit())
			gameRuleDto.setRuleLimit(gameRule.getRuleLimit());
		if(null!=gameRule.getTotalRules())
		gameRuleDto.setTotalRules(gameRule.getTotalRules());
		
		return gameRuleDto;
	}
}
