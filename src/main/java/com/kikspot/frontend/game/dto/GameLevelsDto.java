package com.kikspot.frontend.game.dto;

import com.kikspot.backend.game.model.GameLevels;


/**
 * Created by November 26, 2015
 *  Dto for Game LEvels..
 *  
 *  
 * @author KNS-ACCONTS
 *
 */
public class GameLevelsDto {

	
	private Integer gameLevelId;
	
	private GameDto gameDto;
	
	private String level;
	
	private Integer minCreds;

	public Integer getGameLevelId() {
		return gameLevelId;
	}

	public void setGameLevelId(Integer gameLevelId) {
		this.gameLevelId = gameLevelId;
	}

	public GameDto getGameDto() {
		return gameDto;
	}

	public void setGameDto(GameDto gameDto) {
		this.gameDto = gameDto;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public Integer getMinCreds() {
		return minCreds;
	}

	public void setMinCreds(Integer minCreds) {
		this.minCreds = minCreds;
	}
	
	
	/**
	 * 
	 * @param gameLevels
	 * @return
	 * @throws Exception
	 */
	public static GameLevelsDto populateGameLevelDtos(GameLevels gameLevel)throws Exception{
		GameLevelsDto gameLevelDto=new GameLevelsDto();
		gameLevelDto.setGameLevelId(gameLevel.getGameLevelId());
		gameLevelDto.setLevel(gameLevel.getLevel());
		gameLevelDto.setMinCreds(gameLevel.getMinCreds());
		gameLevelDto.setGameDto(GameDto.populateGameDto(gameLevel.getGame()));
		return gameLevelDto;
	}
	
}
