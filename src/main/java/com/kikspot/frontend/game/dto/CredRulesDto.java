package com.kikspot.frontend.game.dto;

import com.kikspot.backend.game.model.CredRules;

public class CredRulesDto {
	
	private Integer credRuleId;
	private String credType;
	private GameRulesDto gameRuleDto;
	private Integer creds;
	private Integer totalCredRules;
	
	
	
	public Integer getCredRuleId() {
		return credRuleId;
	}
	public void setCredRuleId(Integer credRuleId) {
		this.credRuleId = credRuleId;
	}
	public String getCredType() {
		return credType;
	}
	public void setCredType(String credType) {
		this.credType = credType;
	}
	public GameRulesDto getGameRuleDto() {
		return gameRuleDto;
	}
	public void setGameRuleDto(GameRulesDto gameRuleDto) {
		this.gameRuleDto = gameRuleDto;
	}
	public Integer getCreds() {
		return creds;
	}
	public void setCreds(Integer creds) {
		this.creds = creds;
	}
	public Integer getTotalCredRules() {
		return totalCredRules;
	}
	public void setTotalCredRules(Integer totalCredRules) {
		this.totalCredRules = totalCredRules;
	}
	
	
	public static CredRulesDto populateCredRuleDto(CredRules credRule)	 throws Exception{
		CredRulesDto credRuleDto=new CredRulesDto();
		credRuleDto.setCredRuleId(credRule.getCredRuleId());
		credRuleDto.setCreds(credRule.getCreds());
		credRuleDto.setCredType(credRule.getCredType().name());
		if(null!=credRule.getGameRule()){
			credRuleDto.setGameRuleDto(GameRulesDto.populateGameRulesDto(credRule.getGameRule()));
		}
		if(null!=credRule.getTotalCredRules()){
			credRuleDto.setTotalCredRules(credRule.getTotalCredRules());
		}		
		return credRuleDto;
	}
	

}
