package com.kikspot.frontend.game.service;

import java.util.ArrayList;

import com.kikspot.backend.exceptions.GameRuleNotFoundException;
import com.kikspot.frontend.game.dto.GameRulesDto;
import com.kikspot.frontend.game.dto.UserDashboardDto;

public interface GameService {

	public ArrayList<GameRulesDto> getGameRules()throws GameRuleNotFoundException;
	public UserDashboardDto getUserDashboardInformation(Integer userId)throws Exception;
	
	
}
