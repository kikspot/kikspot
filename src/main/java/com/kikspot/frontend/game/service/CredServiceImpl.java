package com.kikspot.frontend.game.service;

import java.util.ArrayList;
import java.util.Date;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.kikspot.backend.exceptions.UserCredHistoryNotFoundException;
import com.kikspot.backend.exceptions.UserGameCredNotFoundException;
import com.kikspot.backend.game.dao.CredDao;
import com.kikspot.backend.game.model.CredRules;
import com.kikspot.backend.game.model.CredType;
import com.kikspot.backend.game.model.UserCredHistory;
import com.kikspot.backend.game.model.UserGameCreds;
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.frontend.game.dto.CredRulesDto;
import com.kikspot.frontend.game.dto.UserCredHistoryDto;
import com.kikspot.frontend.game.dto.UserGameCredsDto;
/**
 * 
 * Created by Jeevan on Novemeber 26, 2015
 * 
 * Service for CredService..
 * 
 * @author KNS-ACCONTS
 *
 */

@Service("credService")
public class CredServiceImpl implements CredService {
	
	@Resource(name="credDao")
	private CredDao creadDao;

	
	
	
	private static Logger log=Logger.getLogger(CredServiceImpl.class);
	
	
	/**
	 * 
	 * Created by Jeevan on Novemver 26,2015
	 * 
	 * Method to addCredstooUsersAfter User Login...
	 * 
	 * 
	 * @param kikspotUser
	 * @throws Exception
	 * 
	 *  Steps :
	 *  
	 *  1. Saving Creds of Referral.
	 *             i. Get Cred Rule of Refferral
	 *             ii. Get USer Cred Currently
	 *                  User Cred+=Cred Rule Cred
	 *             iii. Update User Cred, User Cred History
	 *             
	 *  2.  For Sign up user
	 *                           i. Get Cred Rule of Sign up
	 *                           ii. Save New User Cred where cred=cred rule cred
	 *                           iii. Update Cred History..
	 * 
	 * 
	 * 
	 */
	
	public void addCredPostUserSignup(KikspotUser kikspotUser)throws Exception{
		log.info("inside addCredPostUserSignup");
		if(null!=kikspotUser.getReferredUser()){
			CredRulesDto credRulesDto=this.getCredRuleBasedonTypeCredType("REFERRAL");
			this.saveorUpdateCredsToUser(kikspotUser.getReferredUser(), true, credRulesDto.getCreds());
		}
		CredRulesDto credRulesDto=this.getCredRuleBasedonTypeCredType("SIGNUP");
		this.saveorUpdateCredsToUser(kikspotUser, false, credRulesDto.getCreds());
	}
	
	
	/**
	 * Created by Jeean on November 26, 2015 
	 * 
	 * 
	 * @param credRule
	 * @return
	 * @throws Exception
	 */
	public CredRulesDto getCredRuleBasedonTypeCredType (String credTypeValue)throws Exception{
		log.info("inside getCredRuleBasedonTypeCredType() ");
		CredType credType=CredType.valueOf(credTypeValue);
		CredRules credRules=this.creadDao.getCredRulesByCredType(credType);
		CredRulesDto credRulesDto=CredRulesDto.populateCredRuleDto(credRules);
		return credRulesDto;
	}
	
	
	
	/**
	 * 
	 * @param kikspotUser
	 * @param isReferral
	 * @param earnedCred
	 * @throws Exception
	 */
	public void saveorUpdateCredsToUser(KikspotUser kikspotUser,Boolean isReferral,Integer earnedCred)throws Exception{
		log.info("inside saveorUpdateCredsToUser()  ");		
		Integer userCred=0;
		UserGameCreds userGameCreds;
		if(isReferral){
			try{
				userGameCreds=this.creadDao.getUserGameCredsByUser(kikspotUser);
				userCred=userGameCreds.getCreds();
			}
			catch(UserGameCredNotFoundException e){
				userGameCreds=new UserGameCreds();
			}
			userGameCreds.setCredType(CredType.REFERRAL);
		}
		else{
			userGameCreds=new UserGameCreds();
			userGameCreds.setCredType(CredType.SIGNUP);
		}		
		userGameCreds.setKikspotUser(kikspotUser);
		userGameCreds.setLevel("BASIC");
		userGameCreds.setCreds(userCred+earnedCred);
		this.creadDao.saveorUpdateUserGameCred(userGameCreds);
		UserCredHistory userCredHistory=new UserCredHistory();
		userCredHistory.setKikspotUser(kikspotUser);
		userCredHistory.setCred(userGameCreds.getCreds());
		userCredHistory.setDate(new Date());
		this.creadDao.saveorUpdateUserCredHistory(userCredHistory);
	}
	
	
	
	
	/**
	 * 
	 * @param kikspotUser
	 * @return
	 * @throws UserGameCredNotFoundException
	 * 
	 *  Created by Jeevan on November 27, 2015
	 *  Get UserGameCredsForUser...
	 *  
	 * 
	 *  
	 */
	public UserGameCredsDto getUserGameCredsofUser(KikspotUser kikspotUser)throws UserGameCredNotFoundException{
		log.info("inside getUserGameCredsofUser() ");
		UserGameCreds userGameCreds=this.creadDao.getUserGameCredsByUser(kikspotUser);
		UserGameCredsDto userGameCredsDto=UserGameCredsDto.populateUserGameCredsDto(userGameCreds);
		return userGameCredsDto;
	}
	
	
	
	/**
	 * 
	 * 
	 * Created by Jeevan on Nov 27, 2015
	 * Methiod to Get Tip Five User Game Creds...
	 * 
	 * @return
	 * @throws UserGameCredNotFoundException
	 */
	public ArrayList<UserGameCredsDto> getTopFiveUserGameCredDtos()throws UserGameCredNotFoundException{
		log.info("inside getTopFiveUserGameCredDtos() ");
		ArrayList<UserGameCreds> userGameCreds=this.creadDao.getTopFiveUserGameCreds();
		ArrayList<UserGameCredsDto> userGameCredDtos=new ArrayList<UserGameCredsDto>();
		for(UserGameCreds userGameCred : userGameCreds){
			UserGameCredsDto userGameCredsDto=UserGameCredsDto.populateUserGameCredsDto(userGameCred);
			userGameCredDtos.add(userGameCredsDto);
		}
		return userGameCredDtos;
	}
	
	
	
	
	/**
	 * 
	 *  
	 *  Created by Jeevan on November 27, 2015
	 *  
	 *  Method to getMostRecentCredHistoryOfUser..
	 * 
	 * @param kikspotUser
	 * @return
	 * @throws UserCredHistoryNotFoundException
	 */
	public UserCredHistoryDto getMostRecentCredHistoryOfUser(KikspotUser kikspotUser)throws UserCredHistoryNotFoundException{
		log.info("inside getMostRecentCredHistoryOfUser() ");
		UserCredHistory userCredHistory=this.creadDao.getMostRecentUserCredHistoryofUser(kikspotUser);
		UserCredHistoryDto userCredHistoryDto=UserCredHistoryDto.populateUserCredHistoryDto(userCredHistory);
		return userCredHistoryDto;
	}
	
	
	
	

	
	
	
}
