package com.kikspot.frontend.game.service;

import java.util.ArrayList;

import com.kikspot.backend.exceptions.UserCredHistoryNotFoundException;
import com.kikspot.backend.exceptions.UserGameCredNotFoundException;
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.frontend.game.dto.CredRulesDto;
import com.kikspot.frontend.game.dto.UserCredHistoryDto;
import com.kikspot.frontend.game.dto.UserGameCredsDto;

public interface CredService {

	public void addCredPostUserSignup(KikspotUser kikspotUser)throws Exception;
	public CredRulesDto getCredRuleBasedonTypeCredType (String credTypeValue)throws Exception;
	public UserGameCredsDto getUserGameCredsofUser(KikspotUser kikspotUser)throws UserGameCredNotFoundException;
	public ArrayList<UserGameCredsDto> getTopFiveUserGameCredDtos()throws UserGameCredNotFoundException;
	public UserCredHistoryDto getMostRecentCredHistoryOfUser(KikspotUser kikspotUser)throws UserCredHistoryNotFoundException;
	
}
