package com.kikspot.frontend.game.service;

import java.util.ArrayList;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.kikspot.backend.exceptions.GameRuleNotFoundException;
import com.kikspot.backend.game.dao.GameDao;
import com.kikspot.backend.game.model.GameRules;
import com.kikspot.backend.user.dao.UserDao;
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.frontend.game.dto.GameRulesDto;
import com.kikspot.frontend.game.dto.UserCredHistoryDto;
import com.kikspot.frontend.game.dto.UserDashboardDto;
import com.kikspot.frontend.game.dto.UserGameCredsDto;
import com.kikspot.frontend.user.dto.KikspotUserDto;




@Service("gameService")
public class GameServiceImpl implements GameService {

	private static Logger log=Logger.getLogger(GameServiceImpl.class);
	
	@Resource(name="gameDao")
	private GameDao gameDao;
	
	
	@Resource(name="credService")
	private CredService credService;
	
	
	@Resource(name="userDao")
	private UserDao userDao;
	
	
	
	
	/**
	 *  Created by Jeevan on November 27, 2015
	 *  
	 *  Method to get UserDashboard Information;..
	 *  
	 * 
	 * @param userId
	 * @throws Exception
	 * 
	 * 
	 *  Steps:
	 *  
	 *  1. Get User by User Id.
	 *  2. Get Creds of User.
	 *  3. GEt PAst cred History of user and decide trend.
	 *  4. Get GameCreds of Top5 Users (Should be user Referrals).
	 *  5. GEt Game Rules
	 *  6. Populate DashboardDto,send..
	 * 
	 * 
	 * 
	 */
	public UserDashboardDto getUserDashboardInformation(Integer userId)throws Exception{
		log.info("inside getUserDashboardInformation() ");
		UserDashboardDto userDashboardDto=new UserDashboardDto();
		KikspotUser kikspotUser=this.userDao.getKikspotUserById(userId);
		UserGameCredsDto userGameCredsDto=this.credService.getUserGameCredsofUser(kikspotUser);
		UserCredHistoryDto userCredHistoryDto=this.credService.getMostRecentCredHistoryOfUser(kikspotUser);
		ArrayList<UserGameCredsDto> userGameCredDtos=this.credService.getTopFiveUserGameCredDtos();		
		try{
			ArrayList<GameRulesDto> gameRuleDtos=this.getGameRules();
			userDashboardDto.setGameRulesDtos(gameRuleDtos);
		}
		catch(GameRuleNotFoundException e){			
		}
		userDashboardDto.setKikspotUserDto(KikspotUserDto.populateKikspotUserDto(kikspotUser));
		
		userDashboardDto.setUserGameCredDto(userGameCredsDto);
		userDashboardDto.setUserGameCredsDtos(userGameCredDtos);
		if(userCredHistoryDto.getCred()<userGameCredsDto.getCreds()){
			userDashboardDto.setTrending("UP");
		}
		else{
			userDashboardDto.setTrending("DOWN");
		}				
		return userDashboardDto;
		
	}
	
	
	
	/**
	 * 
	 * 
	 * Created by Jeevan on November 26, 2015
	 * 
	 * Method to get GameRules...
	 * 
	 * @return
	 * @throws GameRuleNotFoundException
	 * 
	 * 
	 * 
	 */
	
	public ArrayList<GameRulesDto> getGameRules()throws GameRuleNotFoundException{
		log.info("inside getGameRules()");
		ArrayList<GameRules> gameRules=this.gameDao.getGameRulesofGame(1);
		ArrayList<GameRulesDto> gameRuleDtos=new ArrayList<GameRulesDto>();
		
		for(GameRules gameRule:gameRules){
			GameRulesDto gameRulesDto=GameRulesDto.populateGameRulesDto(gameRule);
			gameRuleDtos.add(gameRulesDto);
		}
		return gameRuleDtos;
	}
	
	
	
	
	
}
