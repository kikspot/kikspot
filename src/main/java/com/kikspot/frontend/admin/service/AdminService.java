package com.kikspot.frontend.admin.service;

import java.util.ArrayList;

import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.user.dto.KikspotUserDto;

/**
 *Created By Bhagya On december 03rd,2015 
 *	Interface for the Admin Servcies
 */
public interface AdminService{
	public ArrayList<KikspotUserDto> getAllUsers() throws UserNotFoundException;
}