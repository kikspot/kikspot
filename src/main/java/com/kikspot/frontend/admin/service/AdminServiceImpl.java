package com.kikspot.frontend.admin.service;

import java.util.ArrayList;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kikspot.backend.admin.dao.AdminDao;
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.user.dto.KikspotUserDto;

/**
 * Created By Bhagya On December 03rd,2015
 *	Implementation class for Admin Service
 */
@Transactional
@Service("adminService")
public class AdminServiceImpl implements AdminService{
	
	private static Logger log=Logger.getLogger(AdminServiceImpl.class);
	
	@Resource(name="adminDao")
	private AdminDao adminDao;
	
	/**
	 * Created By Bhagya On december 03rd, 2015
	 * @return
	 * @throws UserNotFoundException
	 * 
	 * Method For Getting the List Of ALl Users
	 */
	public ArrayList<KikspotUserDto> getAllUsers() throws UserNotFoundException{
		log.info("inside AdminServiceImpl -> getAllUsers()");
		ArrayList<KikspotUser> kikspotUsers=this.adminDao.getAllUsers();
		ArrayList<KikspotUserDto> kikspotUserDtos=new ArrayList<KikspotUserDto>();
		for(KikspotUser kikspotUser:kikspotUsers){
			KikspotUserDto kikspotUserDto=KikspotUserDto.populateKikspotUserDto(kikspotUser);
			kikspotUserDtos.add(kikspotUserDto);
		}
		return kikspotUserDtos;
	}
}