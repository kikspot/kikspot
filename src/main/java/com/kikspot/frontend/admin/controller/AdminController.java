package com.kikspot.frontend.admin.controller;

import java.util.ArrayList;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.kikspot.frontend.admin.service.AdminService;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.user.dto.KikspotUserDto;


@Controller
@RequestMapping("/admin")
public class AdminController{
	
	private static Logger log=Logger.getLogger(AdminController.class);
	
	@Resource(name="adminService")
	private AdminService adminService;
	
	/**
	 * Created By Bhagya On December 03rd,2015
	 * 	Method for Showing the list of users to admin
	 */
	@RequestMapping("/viewusers.do")
	public String viewUsersForAdmin(Map<String,Object> map){
		log.info("inside UserController -> viewUsersForAdmin()");
		try{
			ArrayList<KikspotUserDto>  kikspotUserDtos=this.adminService.getAllUsers();
			map.put("kikspotUsers", kikspotUserDtos);
			return "admin/viewUsers";
		}
		catch(UserNotFoundException e){
			String error="Users Not Found";
			map.put("message",error);
			return "error";
		}
		catch(Exception e){
			e.printStackTrace();
			String error="Error Occured While Viewing Users";
			map.put("message",error);
			return "error";
		}
		
	}
}