package com.kikspot.frontend.user.dto;
import java.util.Date;




import org.springframework.format.annotation.DateTimeFormat;

/**
 * Created by bhagya on october 13th,2015
 * 
 * Data Transfer object for kikspot user entity
 */
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.backend.user.model.UserRole;
import com.kikspot.backend.user.model.UserType;

public class KikspotUserDto{
	
	private Integer userId;
	private String username;
	private String password;
	private String emailId;
	private String firstName;
	private String lastName;
	
	private Date dob;
	private Date accountCreationDate;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String country;
	private String zip;
	private String passwordToken;
	private Date passwordTokenExpiryDate;
	private String userRole;
	private String userType;
	private KikspotUserDto referredUser;
	private String profilePicture;
	private Integer totalUsers;
	
	//for support
	private String dateofBirth;
	
	
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public Date getAccountCreationDate() {
		return accountCreationDate;
	}
	public void setAccountCreationDate(Date accountCreationDate) {
		this.accountCreationDate = accountCreationDate;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getPasswordToken() {
		return passwordToken;
	}
	public void setPasswordToken(String passwordToken) {
		this.passwordToken = passwordToken;
	}
	public Date getPasswordTokenExpiryDate() {
		return passwordTokenExpiryDate;
	}
	public void setPasswordTokenExpiryDate(Date passwordTokenExpiryDate) {
		this.passwordTokenExpiryDate = passwordTokenExpiryDate;
	}
	
	public String getUserRole() {
		return userRole;
	}
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public KikspotUserDto getReferredUser() {
		return referredUser;
	}
	public void setReferredUser(KikspotUserDto referredUser) {
		this.referredUser = referredUser;
	}
	public String getProfilePicture() {
		return profilePicture;
	}
	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}
	public Integer getTotalUsers() {
		return totalUsers;
	}
	public void setTotalUsers(Integer totalUsers) {
		this.totalUsers = totalUsers;
	}
	
	
	public String getDateofBirth() {
		return dateofBirth;
	}
	public void setDateofBirth(String dateofBirth) {
		this.dateofBirth = dateofBirth;
	}
	public static KikspotUserDto populateKikspotUserDto(KikspotUser kikspotUser){
		KikspotUserDto kikspotUserDto=new KikspotUserDto();
		kikspotUserDto.setUserId(kikspotUser.getUserId());
		kikspotUserDto.setUsername(kikspotUser.getUsername());
		kikspotUserDto.setEmailId(kikspotUser.getEmailId());
		kikspotUserDto.setPassword(kikspotUser.getPassword());
		kikspotUserDto.setAccountCreationDate(kikspotUser.getAccountCreationDate());
		kikspotUserDto.setAddress1(kikspotUser.getAddress1());
		kikspotUserDto.setAddress2(kikspotUser.getAddress2());
		kikspotUserDto.setCity(kikspotUser.getCity());
		if(null!=kikspotUser.getDob())
			kikspotUserDto.setDob(kikspotUser.getDob());
		kikspotUserDto.setFirstName(kikspotUser.getFirstName());
		kikspotUserDto.setLastName(kikspotUser.getLastName());
		kikspotUserDto.setPasswordToken(kikspotUser.getPasswordToken());
		if(null!=kikspotUser.getPasswordTokenExpiryDate())
			kikspotUserDto.setPasswordTokenExpiryDate(kikspotUser.getPasswordTokenExpiryDate());
		kikspotUserDto.setProfilePicture(kikspotUser.getProfilePicture());
		if(null!=kikspotUser.getReferredUser()){
			kikspotUserDto.setReferredUser(KikspotUserDto.populateKikspotUserDto(kikspotUser.getReferredUser()));
		}
		kikspotUserDto.setState(kikspotUser.getState());
		kikspotUserDto.setTotalUsers(kikspotUser.getTotalUsers());
		kikspotUserDto.setUserRole(kikspotUser.getUserRole().name());
		kikspotUserDto.setUserType(kikspotUser.getUserType().name());
		kikspotUserDto.setZip(kikspotUser.getZip());
		return kikspotUserDto;
	}
	
	
}