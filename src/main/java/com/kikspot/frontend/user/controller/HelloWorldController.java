package com.kikspot.frontend.user.controller;


import org.springframework.ui.ModelMap;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.kikspot.frontend.user.dto.KikspotUserDto;


@RestController
@RequestMapping("/helloworld")

public class HelloWorldController {
 

        @RequestMapping(value = "/hello.do", method = RequestMethod.GET)
        public String hello(ModelMap model) {
        	System.out.println(" inside get method of helloworld");
            model.addAttribute("msg", "JCG Hello World!");
            return "helloworld";

        }

       
        @RequestMapping(value = "/users.do",method = RequestMethod.GET)
        public  KikspotUserDto getAllUsers(){
             KikspotUserDto userDto=new KikspotUserDto();
              	userDto.setUserId(1);
        	 	userDto.setUsername("sai");
        	 	userDto.setPassword("123");
       return userDto;
         }
        
        @RequestMapping(value = "/getjson.do",method = RequestMethod.POST)
        public  KikspotUserDto getJSONData(@RequestBody KikspotUserDto userDto){
        	System.out.println("Id "+userDto.getUserId() +" username "+userDto.getUsername() + "password "+userDto.getPassword());
        	return userDto;
        }
    }
