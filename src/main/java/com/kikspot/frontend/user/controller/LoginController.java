package com.kikspot.frontend.user.controller;
/**
 * Created By Bhagya on October 13th, 2015
 * Maintaining the Login methods
 */
import java.util.Map;



import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kikspot.frontend.common.exception.MailNotSentException;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.common.exception.UserNotSavedOrUpdatedException;
import com.kikspot.frontend.user.dto.KikspotUserDto;
import com.kikspot.frontend.user.service.UserService;

import org.apache.log4j.Logger;

@Controller("webLoginController")
public class LoginController{
	
private static Logger log=Logger.getLogger(LoginController.class);
	
	@Resource(name="userService")
	private UserService userService;
	/**
	 * Created by bhagya on october 13th,2015
	 * Init the login 
	 */
	@RequestMapping(value="/login.do",method=RequestMethod.GET)
	public String login(){
		log.info("inside login");
		
		try{			
			 return "login";
		}
		catch(Exception e){
			e.printStackTrace();
			return "error";
		}
	}
	/**
	 * Created by bhagya on october 13th,2015
	 * @param request
	 * @param map
	 * @return
	 * 
	 * Method for handling the process after login success
	 */
	
	@RequestMapping(value="/loginsuccess.do",method=RequestMethod.GET)
	public String redirectToLoginSuccess(HttpServletRequest request,Map<String, Object> map){
		log.info("inside redirectToLoginSuccess");
		HttpSession session=request.getSession();
		try{			
			Authentication auth=SecurityContextHolder.getContext().getAuthentication();
			String user=auth.getName();
			session.setAttribute("username", user);
			return "dashboard";
		}
		catch(Exception e){
			e.printStackTrace();
			String error="Problem While Login, Please Try Again Later";
			map.put("message",error);
			return "error";
		}
	}
	/**
	 * Created By bhagya On october 19th, 2015
	 * Initialize the Forgot Password
	 */
	@RequestMapping(value="/forgotpassword.do",method=RequestMethod.GET)
	public String initForgotPassword(){
		log.info("inside initForgotPassword()");
		try{
			
			return "forgotPassword";
		}
		catch(Exception e){
			e.printStackTrace();
			return "error";
		}
	}
	/**
	 * Created By Bhagya On October 19th,2015
	 * @param email
	 * @param map
	 * @return
	 * 
	 * Method for process the Forgot Password,
	 * Reset password link to be sent the registered mail
	 */
	@RequestMapping(value="/forgotpassword.do",method=RequestMethod.POST)
	public String processForgotPassword(@RequestParam("emailId") String emailId,Map<String, Object> map){
		log.info("inside processForgotPassword()");
		try{
			if(!emailId.equals("")){
				Integer successMail=this.userService.sendPasswordResetEmail(emailId);
				String msg= "Password reset link has been sent to your registered email, please check your mail.";
				map.put("sucessMessage", msg);
				map.put("title", "Reset Password");
				return "login";
			}
			
		}
		catch(UserNotFoundException e){
			e.printStackTrace();
			String error="User Not Found";
			map.put("message",error);
			return "error";
		}
		catch(UserNotSavedOrUpdatedException e){
			e.printStackTrace();
			String error="User Not Saved Or Updated";
			map.put("message",error);
			return "error";
		}
		catch(MailNotSentException e){
			e.printStackTrace();
			String error="Mail Not Sent";
			map.put("message",error);
			return "error";
		}
		catch(Exception e){
			e.printStackTrace();
			String error="Problem While ForgotPassword, Please Try Again Later";
			map.put("message",error);
			return "error";
		}
		return "forgotPassword";
	}
	/**
	 * Created By bhagya On october 19th,2015
	 * @param token
	 * @param map
	 * @return
	 * 
	 * Method for initiate the Reset Password
	 */
	@RequestMapping(value="/resetpassword.do",method=RequestMethod.GET)
	public String initResetPassword(@RequestParam("token") String token,Map<String,Object> map){
		log.info("inside initResetPassword()");
		try{
			Integer userId = this.userService.getUserIdByPasswordResetToken(token);
			map.put("userId", userId);
			map.put("title", "Reset Password");
			return "resetPassword";
		}
		catch(Exception e){
			e.printStackTrace();
			map.put("errorMessage","This link is not valid any more");
			return "login";
		}
	}
	/**
	 * Created By Bhagya on october 20th, 2015
	 * @param userId
	 * @param password
	 * @param confirmPassword
	 * @param map
	 * @return
	 * 
	 * Method For Saving the reset Password
	 */
	@RequestMapping(value="/resetpassword.do",method=RequestMethod.POST)
	public String performResetPassword(@RequestParam("userId") Integer userId,@RequestParam("password") String password,
			@RequestParam("confirmPassword") String confirmPassword,Map<String,Object> map){
		log.info("inside performResetPassword()");
		try{
			if(password.equals(confirmPassword)){
			this.userService.handlePasswordReset(userId, password);
			map.put("sucessMessage", "Password Reset Successfully");
			}
			else{
				map.put("errorMessage", "Confirmation Password doesn't match Password");
			}
			return "login";
		}
		catch(UserNotFoundException e){
			e.printStackTrace();
			String error="User Not Found";
			map.put("message",error);
			return "error";
		}
		catch(UserNotSavedOrUpdatedException e){
			e.printStackTrace();
			String error="User Not Saved Or Updated";
			map.put("message",error);
			return "error";
		}
		catch(Exception e){
			e.printStackTrace();
			String error="Problem While Reset Password, Please Try Again Later";
			map.put("message",error);
			return "error";
		}
		
	}
	
}

