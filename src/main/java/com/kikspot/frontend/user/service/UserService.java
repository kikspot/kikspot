package com.kikspot.frontend.user.service;

import com.kikspot.frontend.common.exception.MailNotSentException;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.common.exception.UserNotSavedOrUpdatedException;
import com.kikspot.frontend.user.dto.KikspotUserDto;
/**
 * Created By Bhagya On October 13th,2015
 *	Interface for the User service
 */

public interface UserService{
	public Integer sendPasswordResetEmail(String email) throws UserNotFoundException, UserNotSavedOrUpdatedException, MailNotSentException;
	public Integer getUserIdByPasswordResetToken(String token) throws UserNotFoundException;
	public Integer handlePasswordReset(Integer userId,String password) throws UserNotFoundException, UserNotSavedOrUpdatedException;
	public Integer handleFacebookLoginUsers(KikspotUserDto kikspotUserDto) throws UserNotSavedOrUpdatedException;
	public KikspotUserDto getUserByEmailId(String emailId);
	public KikspotUserDto initUserSignUp(KikspotUserDto kikspotUserDto)throws Exception;
	public Integer completeSignUp(KikspotUserDto kikspotUserDto)throws Exception;
	public KikspotUserDto performSocialLogin(KikspotUserDto kikspotUserDto)throws Exception;
	public Integer registerByPassUser()throws Exception;
	public Integer authenticateMobileUser(KikspotUserDto kikspotUserDto) throws Exception;
	
	
}