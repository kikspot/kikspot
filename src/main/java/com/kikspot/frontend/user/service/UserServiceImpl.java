package com.kikspot.frontend.user.service;
/**
 * Created By Bhagya On october 13th,2015
 * Implementation class for UserService Interface
 */
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.kikspot.backend.exceptions.EmailAlreadyExistsException;
import com.kikspot.backend.exceptions.PasswordNotMatchException;
import com.kikspot.backend.exceptions.UserProbableReferralNotFoundException;
import com.kikspot.backend.exceptions.UsernameAlreadyExistsException;
import com.kikspot.backend.user.dao.UserDao;
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.backend.user.model.UserProbableReferral;
import com.kikspot.backend.user.model.UserRole;
import com.kikspot.backend.user.model.UserType;
import com.kikspot.frontend.common.exception.MailNotSentException;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.common.exception.UserNotSavedOrUpdatedException;
import com.kikspot.frontend.common.utility.EmailSender;
import com.kikspot.frontend.game.service.CredService;
import com.kikspot.frontend.user.dto.KikspotUserDto;







import org.apache.log4j.Logger;

@Transactional
@Service("userService")
public class UserServiceImpl implements UserService{
	
	private static Logger log=Logger.getLogger(UserServiceImpl.class);
	
	@Resource(name="userDao")
	private UserDao userDao;
	
	@Resource(name="emailSender")
	private EmailSender emailSender;
	
	
	@Resource(name="credService")
	private CredService credService;
	
	@Autowired
	private org.springframework.security.crypto.password.PasswordEncoder bCryptEncoder;
	
	
	
	
	/**
	 * Created By Bhagya On October 19th,2015
	 * @param email
	 * @throws UserNotFoundException
	 * @throws UserNotSavedOrUpdatedException
	 * 
	 * Method for to send  the password reset email to user
	 * @throws MailNotSentException 
	 */
	public Integer sendPasswordResetEmail(String email) throws UserNotFoundException, UserNotSavedOrUpdatedException, MailNotSentException{
		log.info("inside sendPasswordResetEmail()");
		KikspotUser kikspotUser=this.userDao.getKikspotUserByEmail(email);
		String passwordToken = this.generatePasswordToken();
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, 1);
		Date validity = cal.getTime();
		kikspotUser.setPasswordTokenExpiryDate(validity);
		kikspotUser.setPasswordToken(passwordToken);
		this.userDao.saveOrUpdateKikspotUser(kikspotUser);
		KikspotUserDto kikspotUserDto=KikspotUserDto.populateKikspotUserDto(kikspotUser);
		this.emailSender.sendForgotPasswordMail(kikspotUserDto);
		return 1;
	}
	
	
	
	
	
	
	/**
	 * Created By Bhagya On october 19th,2015
	 * This Method return random UUID
	 * java.util.UUID class represents an immutable universally unique identifier (UUID)
	 * 
	 */
	private String generatePasswordToken() {
		String uuid = UUID.randomUUID().toString();
		String token = uuid.toString().replaceAll("-", "").toUpperCase();
		return token;
	}
	
	
	/**
	 * Created By Bhagya On october 19th,2015
	 * @param token
	 * @return
	 * @throws UserNotFoundException
	 * 
	 * Method For Getting the UserId By Password Reset TOken
	 */
	public Integer getUserIdByPasswordResetToken(String token) throws UserNotFoundException {
		KikspotUser kikspotUser = this.userDao.getKikspotUserByPasswordResetToken(token);
		return kikspotUser.getUserId();
	}
	/**
	 * Created By Bhagya On october 20th, 2015
	 * @param userId
	 * @param password
	 * @return
	 * @throws UserNotFoundException
	 * @throws UserNotSavedOrUpdatedException
	 * 
	 * Method for hadling the process of password reset
	 */
	public Integer handlePasswordReset(Integer userId,String password) throws UserNotFoundException, UserNotSavedOrUpdatedException{
		log.info("inside handlePasswordReset()");
		Integer result=0;
		KikspotUser kikspotUser=this.userDao.getKikspotUserById(userId);
		Date passwordTokenExpiryDate=kikspotUser.getPasswordTokenExpiryDate();
		if(passwordTokenExpiryDate.after(new Date())){
			String encryptedPassword = bCryptEncoder.encode(password);
			kikspotUser.setPassword(encryptedPassword);
			kikspotUser.setPasswordToken(null);
			kikspotUser.setPasswordTokenExpiryDate(null);
			result=	this.userDao.saveOrUpdateKikspotUser(kikspotUser);
		}
		return result;
	}
	
	
	
	/**
	 * Created By Bhagya On October 26th,2015
	 * @param kikspotUserDto
	 * @return
	 * @throws UserNotSavedOrUpdatedException
	 * 
	 * Method for handling the facebook login users
	 *  check the kikspot user already exists or not by emaillId
	 *  If user exists means returns the userId
	 *  If user Not exists means , its create a new user and return the created userId
	 * @throws RoleNotFoundException 
	 */
	public Integer handleFacebookLoginUsers(KikspotUserDto kikspotUserDto) throws UserNotSavedOrUpdatedException{
		log.info("inside handleFacebookLoginUsers()");
		try {
			KikspotUser kikspotUser=this.userDao.getKikspotUserByEmail(kikspotUserDto.getEmailId());
			return kikspotUser.getUserId();
		} 
		catch (UserNotFoundException e) {
			KikspotUser kikspotUser=new KikspotUser();
			kikspotUser.setEmailId(kikspotUserDto.getEmailId());
			kikspotUser.setFirstName(kikspotUserDto.getFirstName());
			kikspotUser.setLastName(kikspotUserDto.getLastName());
			kikspotUser.setUserRole(UserRole.ROLE_USER);
			Integer userId=this.userDao.saveOrUpdateKikspotUser(kikspotUser);
			try {
				this.credService.addCredPostUserSignup(kikspotUser);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return userId;
		}
	}
	
	
	
	/**
	 * Created By Bhagya On October 27th,2015
	 * @param emailId
	 * @return
	 * 
	 * Method For getting the kikspot user by email Id
	 */
	public KikspotUserDto getUserByEmailId(String emailId){
		log.info("inside getUserByEmailId();");
		try{
			Assert.hasText(emailId);
			KikspotUser user=this.userDao.getKikspotUserByEmail(emailId);
			if(user!=null){
				KikspotUserDto userDto=KikspotUserDto.populateKikspotUserDto(user);
				return userDto;
			}
			else{
				return null;
			}		
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("error in user Service "+e.toString());
			return null;
		}
	}
	
	
	
	
	/**
	 * 
	 * Created by Jeevan on November 23, 2015
	 * 
	 * Method to initUserSignup.
	 * 
	 * @param kikspotUserDto
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	public KikspotUserDto initUserSignUp(KikspotUserDto kikspotUserDto)throws Exception{
		log.info("inside initUserSignUp");
		KikspotUser kikspotUser;
		if(null!=kikspotUserDto.getUserId() && kikspotUserDto.getUserId()>0){
			kikspotUser=this.userDao.getKikspotUserById(kikspotUserDto.getUserId());
		}
		else{
			kikspotUser=new KikspotUser();
		}
		try{
			KikspotUser user=this.userDao.getKikspotUserByEmail(kikspotUserDto.getEmailId());
			throw new EmailAlreadyExistsException();
		}
		catch(UserNotFoundException e){	
			
		}
		try{
			KikspotUser user=this.userDao.getKikspotUserByUsername(kikspotUser.getUsername());
			throw new UsernameAlreadyExistsException();
		}
		catch(UserNotFoundException e){
			
		}
		
		kikspotUser.setUsername(kikspotUserDto.getUsername());
		kikspotUser.setEmailId(kikspotUserDto.getEmailId());
		kikspotUser.setPassword(bCryptEncoder.encode(kikspotUserDto.getPassword()));
		kikspotUser.setUserRole(UserRole.ROLE_USER);;
		kikspotUser.setUserType(UserType.LOGGEDIN_USER);
		kikspotUser.setAccountCreationDate(new Date());
		
		kikspotUser=this.getReferralofLoggedinUser(kikspotUser, kikspotUserDto);
		
		//Need to have code related to creds.
		//Get 
		
		Integer userSavedResult=this.userDao.saveOrUpdateKikspotUser(kikspotUser);
		//Need to have logic to send mail..
		
		kikspotUserDto.setUserId(userSavedResult);
		this.credService.addCredPostUserSignup(kikspotUser);
		this.emailSender.sendRegistrationMail(kikspotUserDto);
		return kikspotUserDto;
	}
	
	
	
	
	
	private KikspotUser getReferralofLoggedinUser(KikspotUser kikspotUser,KikspotUserDto kikspotUserDto){
		try{
			UserProbableReferral probableReferral=this.userDao.getUserProbableReferralByEmail(kikspotUserDto.getEmailId());
			kikspotUser.setReferredUser(probableReferral.getKikspotUser());
			
		}
		catch(UserProbableReferralNotFoundException e){
			log.warn("NO USER PROBABLE REFERRAL FOUND AND NEED TO BE ADDED NEW REFERRAL CODE");
		}
		return kikspotUser;
	}
	
	
	
	
	/**
	 * 
	 * Created by Jeevan on November 24, 2015
	 * 
	 * Method to CompleteSignup Process.
	 * 
	 * 
	 * @param kikspotUserDto
	 * @return
	 * @throws Exception
	 * 
	 * 
	 */
	public Integer completeSignUp(KikspotUserDto kikspotUserDto)throws Exception{
		log.info("inside completeSignUp()");
		KikspotUser kikspotUser=this.userDao.getKikspotUserById(kikspotUserDto.getUserId());
		kikspotUser.setFirstName(kikspotUserDto.getFirstName());
		kikspotUser.setLastName(kikspotUserDto.getLastName());
		SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
		kikspotUser.setDob(kikspotUserDto.getDob());
		kikspotUser.setAddress1(kikspotUserDto.getAddress1());
		kikspotUser.setAddress2(kikspotUserDto.getAddress2());
		kikspotUser.setCity(kikspotUserDto.getCity());
		kikspotUser.setState(kikspotUserDto.getState());
		kikspotUser.setZip(kikspotUserDto.getZip());
		Integer userSavedResult=this.userDao.saveOrUpdateKikspotUser(kikspotUser);
		return userSavedResult;
	}
	
	
	
	
	/**
	 * 
	 * Created by Jeevan on November 24, 2015
	 * 
	 * Method to perform Social Login..
	 * 
	 * 
	 * @param kikspotUserDto
	 * @return
	 * @throws Exception
	 */
	public KikspotUserDto performSocialLogin(KikspotUserDto kikspotUserDto)throws Exception{
		log.info("inside performSocialLogin() ");
		
		KikspotUser kikspotUser;
		try{
			kikspotUser=this.userDao.getKikspotUserByEmail(kikspotUserDto.getEmailId());
			KikspotUserDto userDto=KikspotUserDto.populateKikspotUserDto(kikspotUser);
			return userDto;
		}
		catch(UserNotFoundException e){
			log.info("User Logged in for first time ");
			if(null!=kikspotUserDto.getUserId() && kikspotUserDto.getUserId()>0){
				kikspotUser=this.userDao.getKikspotUserById(kikspotUserDto.getUserId());
			}
			else{
				kikspotUser=new KikspotUser();
			}
			kikspotUser.setUsername(kikspotUserDto.getUsername());
			kikspotUser.setEmailId(kikspotUserDto.getEmailId());
			kikspotUserDto.setFirstName(kikspotUserDto.getFirstName());
			kikspotUserDto.setLastName(kikspotUserDto.getLastName());
			kikspotUserDto.setAccountCreationDate(new Date());
			kikspotUser.setUserRole(UserRole.ROLE_USER);
			kikspotUser.setUserType(UserType.SOCIAL_USER);
			
			kikspotUser=this.getReferralofLoggedinUser(kikspotUser, kikspotUserDto);
					
			Integer userSavedResult=this.userDao.saveOrUpdateKikspotUser(kikspotUser);
			//Need to have logic to send mail..
			kikspotUserDto.setUserId(userSavedResult);
			this.credService.addCredPostUserSignup(kikspotUser);
			this.emailSender.sendRegistrationMail(kikspotUserDto);
			return kikspotUserDto;
		}		
	}
	
	
	
	
	
	
	/**
	 * Created by Jeevan on Novemeber 24, 2015
	 * Method to registerByPassUser..
	 * 
	 * @return
	 * @throws Exception
	 */
	public Integer registerByPassUser()throws Exception{
		log.info("inside registerByPassUser() ");
		KikspotUser kikspotUser=new KikspotUser();
		kikspotUser.setAccountCreationDate(new Date());
		kikspotUser.setUserRole(UserRole.ROLE_USER);
		kikspotUser.setUserType(UserType.BYPASS_USER);		
		Integer userSavedResult=this.userDao.saveOrUpdateKikspotUser(kikspotUser);
		return userSavedResult;
		
	}
	
	
	
	/**
	 * Created by Jeevan on 
	 * @param kikspotUserDto
	 * @return
	 * @throws Exception
	 * 
	 * 
	 */
	public Integer authenticateMobileUser(KikspotUserDto kikspotUserDto) throws Exception{
		log.info("inside authenticateMobileUser() ");
		KikspotUser kikspotUser=this.userDao.getKikspotUserforAuthentication(kikspotUserDto.getUsername());
		if(bCryptEncoder.matches(kikspotUserDto.getPassword(), kikspotUser.getPassword())){
			return kikspotUser.getUserId();
		}
		else{
			throw new PasswordNotMatchException();
		}	
	}
	
	
	
	
	
	
	
	
	
	
	
}