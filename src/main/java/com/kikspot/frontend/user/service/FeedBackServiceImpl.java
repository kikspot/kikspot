package com.kikspot.frontend.user.service;

/** 
 * 
 * created by Firdous 24-11-2015
 * Service class to handle User feedback
 * 
 * 
 */

import java.util.Date;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.kikspot.backend.user.dao.UserDao;
import com.kikspot.backend.user.model.KikspotUser;
import com.kikspot.backend.useraction.dao.FeedbackDao;
import com.kikspot.backend.useraction.model.Feedback;
import com.kikspot.frontend.common.utility.EmailSender;
import com.kikspot.frontend.mobile.controller.FeedBackController;
import com.kikspot.frontend.user.dto.KikspotUserDto;


@Transactional
@Service("feedbackService")
public class FeedBackServiceImpl implements Feedbackservice {
    
	private static Logger log = Logger.getLogger(FeedBackServiceImpl.class);
	
	@Resource(name="feedbackDao")
	private FeedbackDao feedbackDao;
	
	@Resource(name="userDao")
	private UserDao userDao;
	
	@Resource(name = "emailSender")
	private EmailSender emailSender;
	/**
	 * 
	 * method to get the feedback from a user and save it inside the database
	 * 
	 */
	@Override
	public Integer getFeedback( String feedbackMessage,Integer userId) throws Exception {
		log.info("inside FeedBackServiceImpl -> getFeedback()");
		Feedback feedback=new Feedback();
		KikspotUser user=this.userDao.getKikspotUserById(userId);
		feedback.setFeedbackMessage(feedbackMessage);
		feedback.setKikspotUser(user);
		feedback.setFeedbackDate(new Date());
		Integer result=this.feedbackDao.saveFeedBack(feedback);
		return result;
	}
    
	
	/**
	 * 
	 * method to show the feedback of a user by accepting user id
	 * 
	 */
	@Override
	public String showFeedback(Integer userId) throws Exception  {
		log.info("inside FeedBackServiceImpl -> showFeedback()");
		Feedback feedback=this.feedbackDao.getFeedback(userId);
		String message=feedback.getFeedbackMessage();
		return message;
		
	}
    
	/**
	 * 
	 * 
	 * method to get the total number of feedbacks
	 * 
	 * 
	 */
	
	public Integer getTotalResults() throws Exception {
		log.info("inside FeedBackServiceImpl -> getTotalResults()");
		Integer totalResults=this.feedbackDao.getTotalFeedbacks();
		return totalResults;
		
	}

    /**
     * 
     * created by Firdous on 25-11-2015
     * method to send the response mail to the user who send the feedback
     * @throws Exception 
     * 
     * Modification done by bhagya on december 03rd, 2015
     * 	Populate the kikspotuser Dto and send at sendEmailToUser service
     */
	@Override
	public Integer sendFeedbackMail(Integer userId) throws Exception {
		log.info("inside FeedBackServiceImpl -> sendFeedbackMail()");
		KikspotUser user=this.userDao.getKikspotUserById(userId);
		KikspotUserDto kikspotUserDto=KikspotUserDto.populateKikspotUserDto(user);
		this.emailSender.sendEmailToUser(kikspotUserDto);
		return 1;
		
	}

    /**
     * 
     * created by firdous on 26-11-2015
     * method to delete the feedback of a user
     * 
     * 
     */
	@Override
	public void deleteFeedBack(Integer user_id) throws Exception {
		log.info("inside FeedBackServiceImpl -> deleteFeedBack()");
		Feedback feedback=this.feedbackDao.getFeedback(user_id);
		this.feedbackDao.deleteFeedback(feedback);
		
	}
	
	
	
	/**
	 * Created By firdous
	 * Method For handling, send a feeback Info mail to admin
	 */

	@Override
	public Integer sendFeedBackInfoToAdmin(Integer user_id, String feedbackMessage) throws Exception {
		log.info("inside FeedBackServiceImpl -> sendFeedBackInfoToAdmin()");
		KikspotUser user=this.userDao.getKikspotUserById(user_id);
		KikspotUserDto userDto=KikspotUserDto.populateKikspotUserDto(user);
		this.emailSender.feedbackInfoToAdmin(userDto,feedbackMessage);
		return 1;
	}
    
}

