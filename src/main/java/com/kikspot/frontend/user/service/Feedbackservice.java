package com.kikspot.frontend.user.service;

public interface Feedbackservice {
	public Integer getFeedback(String feedbackMessage,Integer user_id) throws Exception;
	
	
	public String showFeedback(Integer user_id) throws Exception;
	
	
	public Integer getTotalResults() throws Exception;


	public Integer sendFeedbackMail(Integer user_id) throws Exception;


	public void deleteFeedBack(Integer user_id) throws Exception;


	public Integer sendFeedBackInfoToAdmin(Integer user_id, String feedbackMessage) throws Exception;
	

}
