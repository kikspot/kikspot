package com.kikspot.frontend.yelp;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.scribe.builder.ServiceBuilder;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

import com.kikspot.frontend.yelp.dto.BusinessesDto;
import com.kikspot.frontend.yelp.dto.CoordinateDto;
import com.kikspot.frontend.yelp.dto.LocationDto;
import com.kikspot.frontend.yelp.dto.SpanDto;

import twitter4j.org.json.JSONArray;
import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;

/**
 * Example for accessing the Yelp API.
 */
public class Yelp {

  OAuthService service;
  Token accessToken;

  /**
   * Setup the Yelp API OAuth credentials.
   *
   * OAuth credentials are available from the developer site, under Manage API access (version 2 API).
   *
   * @param consumerKey Consumer key
   * @param consumerSecret Consumer secret
   * @param token Token
   * @param tokenSecret Token secret
   */
  public Yelp(String consumerKey, String consumerSecret, String token, String tokenSecret) {
    this.service = new ServiceBuilder().provider(YelpApi2.class).apiKey(consumerKey).apiSecret(consumerSecret).build();
    this.accessToken = new Token(token, tokenSecret);
  }

  /**
   * Search with term and location.
   *
   * @param term Search term
   * @param latitude Latitude
   * @param longitude Longitude
   * @return JSON string response
   */
  public String search(String term, double latitude, double longitude) {
    OAuthRequest request = new OAuthRequest(Verb.GET, "http://api.yelp.com/v2/search");
    request.addQuerystringParameter("term", term);
    request.addQuerystringParameter("ll", latitude + "," + longitude);
    this.service.signRequest(this.accessToken, request);
    Response response = request.send();
    return response.getBody();
  }
  
  public String locationSearch(String term, String location) {
	    OAuthRequest request = new OAuthRequest(Verb.GET, "http://api.yelp.com/v2/search");
	    request.addQuerystringParameter("term", term);
	    request.addQuerystringParameter("location", location);
	    this.service.signRequest(this.accessToken, request);
	    Response response = request.send();
	    return response.getBody();
	  }

  // CLI
  public static void main(String[] args)  {
    // Update tokens here from Yelp developers site, Manage API access.
    String consumerKey = "PvzCf1t8yTz_0idLM2kUyw";
    String consumerSecret = "_BUHM8VReR2sIaCPyACAONbEaOY";
    String token = "0bl4POe6uLjQF3HwTjJjwfTtXv7VJzq4";
    String tokenSecret = "QLLYv9wmhTCbamPoAx7vEOsk_RE";

    Yelp yelp = new Yelp(consumerKey, consumerSecret, token, tokenSecret);
    String response = yelp.locationSearch("pubs", "San Francisco");
    //String response = yelp.search("bars", 30.361471, -87.164326);
    System.out.println(response);
  
    //JSON Response Parsing
    try{
    SpanDto spanDto=new SpanDto();
   		JSONObject obj = new JSONObject(response);				
   			JSONArray businessesArray = (JSONArray) obj.get("businesses");
   			ArrayList<BusinessesDto> businessesDtos=new ArrayList<BusinessesDto>();
   			for (int i = 0; i < businessesArray.length(); i++) {
   				
   				//Business Dto
   				BusinessesDto businessesDto=new BusinessesDto();
   				JSONObject businessObject=businessesArray.getJSONObject(i);
   				String id=businessObject.getString("id");
   				String name = businessObject.getString("name");
   				Double rating= businessObject.getDouble("rating");
   				Integer review_count=businessObject.getInt("review_count");
   				String phone=null;
   				String display_phone=null;
   				if(!businessObject.isNull("phone")){
   				 phone=businessObject.getString("phone");
   				}
   				if(!businessObject.isNull("display_phone")){
   				 display_phone=businessObject.getString("display_phone");
   				}
   				
   				String snippet_text=businessObject.getString("snippet_text");
   				String url=businessObject.getString("url");
   				String mobile_url=businessObject.getString("mobile_url");
   				String snippet_image_url=businessObject.getString("snippet_image_url");
   				String image_url=businessObject.getString("image_url");
   				String rating_img_url=businessObject.getString("rating_img_url");
   				String rating_img_url_small=businessObject.getString("rating_img_url_small");
   				String rating_img_url_large=businessObject.getString("rating_img_url_large");
   				
	   				//location Dto
	   				JSONObject locationObject=businessObject.getJSONObject("location");
	   				LocationDto locationDto=new LocationDto();
	   				if(!locationObject.isNull("cross_streets")){
	   					String cross_streets=locationObject.getString("cross_streets");
	   				}
	   				String city=locationObject.getString("city");
	   				JSONArray displayAddressArray=locationObject.getJSONArray("display_address");
	   				List<String> dispaly_address=new ArrayList<String>();
	   				for(int j=0;j<displayAddressArray.length();j++){
	   					String displayAddress= (String) displayAddressArray.get(j);
	   					dispaly_address.add(displayAddress);
	   				}
	   				String postal_code=locationObject.getString("postal_code");
	   				String country_code=locationObject.getString("country_code");
	   				JSONArray addressArray=locationObject.getJSONArray("address");
	   				List<String> address=new ArrayList<String>();
	   				for(int k=0;k<addressArray.length();k++){
	   					String addressString= (String) addressArray.get(k);
	   					address.add(addressString);
	   				}
	   				String state_code=locationObject.getString("state_code");
	   				
		   				//coordinate Dto
		   				JSONObject coordinateObject=locationObject.getJSONObject("coordinate");
		   				CoordinateDto coordinateDto=new CoordinateDto();
		   				Double latitude=coordinateObject.getDouble("latitude");
		   				Double longitude=coordinateObject.getDouble("longitude");
		   			
		   				
		   				coordinateDto.setLatitude(latitude);
		   				coordinateDto.setLongitude(longitude);
		   			
	   				
	   				locationDto.setCity(city);
	   				locationDto.setAddress(dispaly_address);
	   				locationDto.setPostal_code(postal_code);
	   				locationDto.setCountry_code(country_code);
	   				locationDto.setAddress(address);
	   				locationDto.setCoordinate(coordinateDto);
	   				locationDto.setState_code(state_code);
	   				
   				businessesDto.setId(id);
   				businessesDto.setName(name);
   				businessesDto.setRating(rating);
   				businessesDto.setReview_count(review_count);
   				businessesDto.setPhone(phone);
   				businessesDto.setDisplay_phone(display_phone);
   				businessesDto.setLocation(locationDto);
   				businessesDto.setSnippet_text(snippet_text);
   				businessesDto.setSnippet_image_url(snippet_image_url);
   				businessesDto.setUrl(url);
   				businessesDto.setImage_url(image_url);
   				businessesDto.setMobile_url(mobile_url);
   				businessesDto.setRating_img_url(rating_img_url);
   				businessesDto.setRating_img_url_small(rating_img_url_small);
   				businessesDto.setRating_img_url_large(rating_img_url_large);
   				
   			businessesDtos.add(businessesDto);
   			
   			
     		}
   			
   		spanDto.setBusinesses(businessesDtos);
    }
    catch(JSONException e){
    	e.printStackTrace();
    }
  }
}