package com.kikspot.frontend.yelp.dto;

import java.util.ArrayList;

public class SpanDto{
	private Double latitude_delta;
    private Double longitude_delta;
    private CenterDto center;
    private Integer total;
    private ArrayList<BusinessesDto> businesses;
    
	public Double getLatitude_delta() {
		return latitude_delta;
	}
	public void setLatitude_delta(Double latitude_delta) {
		this.latitude_delta = latitude_delta;
	}
	public Double getLongitude_delta() {
		return longitude_delta;
	}
	public void setLongitude_delta(Double longitude_delta) {
		this.longitude_delta = longitude_delta;
	}
	public CenterDto getCenter() {
		return center;
	}
	public void setCenter(CenterDto center) {
		this.center = center;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public ArrayList<BusinessesDto> getBusinesses() {
		return businesses;
	}
	public void setBusinesses(ArrayList<BusinessesDto> businesses) {
		this.businesses = businesses;
	}
	
    
}