package com.kikspot.frontend.yelp.dto;

public class RegionDto{
	private SpanDto span;

	public SpanDto getSpan() {
		return span;
	}

	public void setSpan(SpanDto span) {
		this.span = span;
	}
	
}