package com.kikspot.frontend.common.exception;
/***
 * Created By Bhagya On october 19th,2015
 *	This Exception Handles the condition when the user not found
 */
public class UserNotFoundException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "User Not Found";
	}
}