package com.kikspot.frontend.common.utility;

import com.kikspot.frontend.common.exception.MailNotSentException;
import com.kikspot.frontend.user.dto.KikspotUserDto;

public interface EmailSender{
	public void sendForgotPasswordMail(final KikspotUserDto userDto) throws MailNotSentException;
	public void sendRegistrationMail(final KikspotUserDto kikspotUserDto)throws MailNotSentException;
	public void feedbackInfoToAdmin(final KikspotUserDto userDto, final String feedbackMessage) throws Exception;
	public void sendEmailToUser(final KikspotUserDto kikspotUserDto) throws Exception;
	

}