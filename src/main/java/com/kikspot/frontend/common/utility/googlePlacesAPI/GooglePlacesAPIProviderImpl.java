package com.kikspot.frontend.common.utility.googlePlacesAPI;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import se.walkercrou.places.GooglePlaces;
import se.walkercrou.places.Param;
import se.walkercrou.places.Photo;
import se.walkercrou.places.Place;
import se.walkercrou.places.exception.GooglePlacesException;
import se.walkercrou.places.exception.NoResultsFoundException;

import com.kikspot.frontend.recommendation.dto.RecommendationLocationDto;


/**
 * Created By Bhagya On November 23rd,2015
 * Accessing The Google Places API Service, get the Information about places
 *
 */
@Transactional
@Service("googlePlacesAPIProvider")
public class GooglePlacesAPIProviderImpl implements GooglePlacesAPIProvider{
	
	private static Logger log=Logger.getLogger(GooglePlacesAPIProviderImpl.class);
	
	private String photoURL;
	private String apiKey;
	
	
	public String getPhotoURL() {
		return photoURL;
	}

	public void setPhotoURL(String photoURL) {
		this.photoURL = photoURL;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	/**
	 * Created By Bhagya On November 23rd,2015
	 * @param latitude
	 * @param longitude
	 * @return
	 * 
	 * Method For Getting the Near by locations by Lat And Lng
	 * 	Accessing the Google Places API by API Key
	 * 	Getting the near by places by lat and lng while interacting with google places api
	 * 	populate the Recommendation Location Dto
	 * @throws Exception 
	 */
	public ArrayList<RecommendationLocationDto> getLocationsByLatAndLngForRecommendations(Double latitude,Double longitude) throws Exception{
		log.info("GooglePlaceAPIProviderImpl->getLocationsByLatAndLngForRecommendations()");
		ArrayList<RecommendationLocationDto> recommendationLocationDtos=new ArrayList<RecommendationLocationDto>();
		try{
			GooglePlaces client = new GooglePlaces(apiKey);
			List<Place> places = client.getNearbyPlaces(latitude, longitude, GooglePlaces.MAXIMUM_RESULTS);
			for(Place place:places)
			{
				RecommendationLocationDto recommendationLocationDto=new RecommendationLocationDto();
				recommendationLocationDto.setLocationAPIId(place.getPlaceId());
				recommendationLocationDto.setLocationName(place.getName());
				recommendationLocationDto.setLatitude(place.getLatitude());
				recommendationLocationDto.setLongitude(place.getLongitude());
				recommendationLocationDto.setIconUrl(place.getIconUrl());
				recommendationLocationDtos.add(recommendationLocationDto);
			}
			return recommendationLocationDtos;
		}
		
		catch(GooglePlacesException e){
			e.printStackTrace();
			if(e.getMessage().contains("ZERO_RESULTS")){
				throw new NoResultsFoundException();
			}
		throw new Exception();
		}
		catch(Exception e){
			//e.printStackTrace();
			throw e;
		}
		
	}
	/**
	 * Created By Bhagya On November 25th,2015
	 * @param LocationAPIId
	 * @return
	 * @throws Exception
	 * 
	 * Method For getting the Loctaion Details By Location APIId while accessing the google places api
	 */
	
	public RecommendationLocationDto getLocationDetailsByLocationAPIId(String locationAPIId) throws Exception{
		log.info("GooglePlaceAPIProviderImpl->getLocationDetailsByLocationAPIId()");
		try{
			GooglePlaces client = new GooglePlaces(apiKey);
			Place placeDetails=client.getPlaceById(locationAPIId);
			RecommendationLocationDto recommendationLocationDto=new RecommendationLocationDto();
			recommendationLocationDto.setLocationAPIId(placeDetails.getPlaceId());
			recommendationLocationDto.setLocationName(placeDetails.getName());
			recommendationLocationDto.setLatitude(placeDetails.getLatitude());
			recommendationLocationDto.setLongitude(placeDetails.getLongitude());
			recommendationLocationDto.setRatings(Double.toString(placeDetails.getRating()));
			recommendationLocationDto.setAddress(placeDetails.getAddress());
			recommendationLocationDto.setPhoneNo(placeDetails.getPhoneNumber());
			recommendationLocationDto.setIconUrl(placeDetails.getIconUrl());
			recommendationLocationDto.setIsNew(false);
			 List<Photo> photos = placeDetails.getPhotos();
			 if(photos.size()>0){
				 String photoReference=photos.get(0).getReference();
				 String imageUrl=photoURL+photoReference+"&key="+apiKey;
				 recommendationLocationDto.setImage(imageUrl);
			 }
			 recommendationLocationDto.setUrl(placeDetails.getWebsite());
			return recommendationLocationDto;
		}
		catch(GooglePlacesException e){
			e.printStackTrace();
			if(e.getMessage().contains("ZERO_RESULTS")){
				throw new NoResultsFoundException();
			}
		throw new Exception();
		}
		catch(Exception e){
			//e.printStackTrace();
			throw e;
		}
		
	}
}