package com.kikspot.frontend.common.utility.googlePlacesAPI;

import java.util.ArrayList;

import com.kikspot.frontend.recommendation.dto.RecommendationLocationDto;

/**
 * Created By Bhagya On November 23rd,2015
 * Interface for Google Places API provider
 */
public interface GooglePlacesAPIProvider{
	public ArrayList<RecommendationLocationDto> getLocationsByLatAndLngForRecommendations(Double latitude,Double longitude) throws Exception;
	public RecommendationLocationDto getLocationDetailsByLocationAPIId(String locationAPIId) throws Exception;
}

