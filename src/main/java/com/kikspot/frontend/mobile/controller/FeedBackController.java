package com.kikspot.frontend.mobile.controller;

/**
 * 
 * 
 * created by Firdous on 23-11-2015
 * 
 * controller to handle the feedback messages of a user
 * 
 * 
 */



import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.mail.MailSendException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import twitter4j.org.json.JSONObject;
import com.kikspot.frontend.common.exception.MailNotSentException;
import com.kikspot.backend.exceptions.FeedBackNotFoundException;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.common.utility.JsonParser;
import com.kikspot.frontend.user.service.Feedbackservice;
import com.kikspot.frontend.user.service.UserService;





@RestController
@RequestMapping("/mobile")
public class FeedBackController {
	
	private static Logger log = Logger.getLogger(FeedBackController.class);
	 
    @Resource(name="feedbackService")	
    private Feedbackservice feedbackService;
    
    
    
    @Resource(name="userService")
	private UserService userService;
    
   
    
    
    /**
     * @author Firdous
     * @param request
     * @param response
     * @return json object with feedback message
     * @throws Exception
     */
    
    @RequestMapping(value="/feedback.do")
    public String getFeedBack(HttpServletRequest request,HttpServletResponse response) throws Exception  {
    	   log.info("inside getFeedback()");
    	  String error="you are unable to send the feedback";
    	  JSONObject obj=new JSONObject();
		  String status="feedback message successfully recieved"; 
		  try {
		     JSONObject json=JsonParser.getJson(request, response);
		     String feedbackMessage=json.getString("feedbackMessage");
		     Integer userId=json.getInt("userId");
    	     Integer result=this.feedbackService.getFeedback(feedbackMessage,userId);
		     if(result>0) {
		    	Integer successMail=this.feedbackService.sendFeedbackMail(userId);
		    	Integer succesMail2=this.feedbackService.sendFeedBackInfoToAdmin(userId,feedbackMessage);
		    	status="Thank you for the feedback";
				obj.accumulate("STATUS",status);
		      }
	    
    	    } 
			catch(UserNotFoundException e)	{
				error="User does not exist";
				obj.accumulate("ERROR",error);
			}
		
			catch(MailNotSentException e)	{
				error="Mail Not Sent";
				obj.accumulate("ERROR",error);
				
			}
		    catch(Exception e)  {
			     e.printStackTrace();
				 error = "Error occured While Getting the Feedback From User";
				 obj.accumulate("ERROR", error);
			 }
		  
		  String result = obj.toString();
		  log.info("response : " + result);
		  System.out.println("response : " + result);
		  
		  return result;
      }
    
    
    
     /**
      * @author Firdous
      * @param request
      * @param response
      * @return display the feedback message of a user
      * @throws Exception
      */
    
      @RequestMapping(value="/showfeedback.do")
      public String showFeedBack(HttpServletRequest request,HttpServletResponse response) throws Exception  {
    	   	log.info("inside showfeedback( )");
    	   	String error="no feedback present";
    	   	JSONObject obj=new JSONObject();
    	
		try  {
			JSONObject json=JsonParser.getJson(request, response);
			Integer user_id=json.getInt("userId");
			String result=this.feedbackService.showFeedback(user_id);
			obj.accumulate("user Feed Back", result);
		 }
		  
		 catch(FeedBackNotFoundException e) {
			 error="No Feedbacks Found For the User";
			 obj.accumulate("ERROR",error);
		 }
		 catch(Exception e){
			 e.printStackTrace();
			 error = "Error occured While Showing Feedbacks";
			 obj.accumulate("ERROR", error);
		}
		
		 String result = obj.toString();
		 log.info("response : " + result);
		 System.out.println("response : " + result);
		 return result;
    	
    }
      
      
      /**
       * @author Firdous
       * @param request
       * @param response
       * @return total number of feedbacks
       * @throws Exception
       */
      
      
      @RequestMapping(value="/totalfeedbacks.do")
      public String showTotalfeedback(HttpServletRequest request,HttpServletResponse response) throws Exception  {
    	log.info("inside showTotalfeedback()");
      	String error="no feedback present";
      	System.out.println("inside show total feedback");
        JSONObject obj=new JSONObject();
       	try {
      		 Integer result=this.feedbackService.getTotalResults();
      		 JSONObject editJson=new JSONObject();
             editJson.accumulate("toatlfeedbacks",result);
	    	 obj.accumulate("total number of feedbacks", editJson);
		}
		
		 catch(Exception e){
			 e.printStackTrace();
			 error = "Error occured while Showing Total Feedbacks";
			 obj.accumulate("ERROR", error);
			
		}
		String result = obj.toString();
		log.info("response : " + result);
		System.out.println("response : " + result);
		return result;
    	
      	
      }
      
      /**
       * Created By Firdous
       * @param request
       * @param response
       * @return
       * @throws Exception
       * 
       * Method for delete feedback
       */
      @RequestMapping(value = "/deletefeedback.do")
  	  public String deleteUserFeedBack(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	  log.info("inside deleteUserMessage()");
  		JSONObject obj = new JSONObject();
  		try  
  		{
  			JSONObject json = JsonParser.getJson(request, response);
  			Integer userId = json.getInt("userId");
  			this.feedbackService.deleteFeedBack(userId);
  			obj.accumulate("STATUS", "User Feedback Deleted");
   		 }

  		 catch (FeedBackNotFoundException e) {
  			log.error("Error in Deleting Feedback" + e.toString());
  			obj.accumulate("ERROR", "No Feedback found for this user");
  		  } 
  		  catch (Exception e) {
  			e.printStackTrace();
  			log.error("Some Error Occurred While Deleting Feedback" + e.toString());
  			obj.accumulate("ERROR", "Some Error Occurred While Deleting Feedback");
  		  }
  		  return obj.toString();

  	 }

    	 
}
