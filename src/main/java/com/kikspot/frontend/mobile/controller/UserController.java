package com.kikspot.frontend.mobile.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.kikspot.backend.exceptions.EmailAlreadyExistsException;
import com.kikspot.backend.exceptions.PasswordNotMatchException;
import com.kikspot.backend.exceptions.UsernameAlreadyExistsException;
import com.kikspot.frontend.common.exception.UserNotFoundException;
import com.kikspot.frontend.user.dto.KikspotUserDto;
import com.kikspot.frontend.user.service.UserService;


/**
 * 
 *  Created by Jeevan on November 23, 2015
 *  
 *  Controller for all User Related aCtivities..
 *  
 * 
 * @author KNS-ACCONTS
 *
 */
@RestController
@RequestMapping("/mobile")
public class UserController {
	
	
	private static Logger log=Logger.getLogger(UserController.class);
	

	@Resource(name="userService")
	private UserService userService;
	
	
	
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	  
	  dateFormat.setLenient(false);	
	   // true passed to CustomDateEditor constructor means convert empty String to null
	  binder.registerCustomEditor(Date.class,new CustomDateEditor(dateFormat, true));
	  
	 
	}
	
	
	/**
	 * 
	 * Created by Jeevan on November 23, 2015
	 * 
	 *  Method to initiate Signup..
	 *  
	 * @param request
	 * @param response
	 * @param userDto
	 * @return
	 * @throws JSONException 
	 */
	@RequestMapping("/signup.do")
	public String signupUser(HttpServletRequest request,HttpServletResponse response,@RequestBody KikspotUserDto kikspotUserDto) throws JSONException{
		log.info("inside singUpUser() ");
		JSONObject json=new JSONObject();
		try{
			KikspotUserDto userDto=this.userService.initUserSignUp(kikspotUserDto);
			json.accumulate("STATUS", "Success");
			json.accumulate("UserId", userDto.getUserId());
			if(null!=userDto.getReferredUser()){
				json.accumulate("Referral", true);
			}
			else{
				json.accumulate("Referral", false);
			}
		}
		catch(EmailAlreadyExistsException e){
			log.error("Email Already Exists "+e.toString());
			json.accumulate("ERROR", "Email Already Exists");
			
		}
		catch(UsernameAlreadyExistsException e){
			log.error("Username Already Exists "+e.toString());
			json.accumulate("ERROR", "Username Already Exists");
			
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While User Sign Up "+e.toString());
			json.accumulate("ERROR","Error Signingup User");
		}
		return json.toString();
		
	}
	
	
	
	
	
	/**
	 * Created by Jeevan on November 24, 2015
	 * Method to complete Signup Process.
	 * 
	 * 
	 * @param request
	 * @param response
	 * @param kikspotUserDto
	 * @return
	 * @throws JSONException 
	 */
	@RequestMapping("/completesignup.do")
	public String completeSignup(HttpServletRequest request,HttpServletResponse response,@RequestBody KikspotUserDto kikspotUserDto)throws JSONException{
		log.info("inside completeSignup()");
		JSONObject json=new JSONObject();
		try{
			SimpleDateFormat dateFormat=new SimpleDateFormat("MM/dd/yyyy");
			kikspotUserDto.setDob(dateFormat.parse(kikspotUserDto.getDateofBirth()));
			Integer savedResult=this.userService.completeSignUp(kikspotUserDto);
			if(savedResult>0)
				json.accumulate("STATUS", "Registration Completed Successfully");			
			else
				throw new Exception();
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While Completing Registration");
			json.accumulate("ERROR", "Unexpected Error While Completing Signup, Please Try Again");			
		}
		return json.toString();
	}
	
	
	
	/**
	 * 
	 *  Created by Jeevan on November 24, 2015
	 *  
	 *  Method to PerformSocialLogin..()
	 * 
	 * @param request
	 * @param response
	 * @param kikspotDto
	 * @return
	 * @throws JsonGenerationException
	 * @throws JSONException 
	 */
	@RequestMapping("/sociallogin.do")
	public String performSocialLogin(HttpServletRequest request,HttpServletResponse response,@RequestBody KikspotUserDto kikspotUserDto)throws JsonGenerationException, JSONException{
		log.info("inside performSocialLogin() ");
		JSONObject json=new JSONObject();
		try{
			KikspotUserDto userDto=this.userService.performSocialLogin(kikspotUserDto);
			json.accumulate("STATUS", "Success");
			json.accumulate("UserId", userDto.getUserId());
			
			if(null!=userDto.getFirstName() && userDto.getFirstName().trim().length()>0){
				if(null!=userDto.getReferredUser()){
					json.accumulate("Referral", true);
				}
				else{
					json.accumulate("Referral", false);
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While User Sign Up "+e.toString());
			json.accumulate("ERROR","Error Signingup User");
		}
		return json.toString();
	}
	
	
	
	/**
	 * 
	 * Created by Jeevan on November 14, 2015
	 * 
	 *  Method to byPassUser...
	 *  
	 * 
	 * 
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws JSONException
	 */
	@RequestMapping("/bypassuser.do")
	public String bypassUser(HttpServletRequest request,HttpServletResponse response)throws JSONException{
		JSONObject json=new JSONObject();
		try{
			Integer userSavedResult=this.userService.registerByPassUser();
			json.accumulate("STATUS", "Success");
			json.accumulate("UserId", userSavedResult);
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While User Sign Up Bypass User "+e.toString());
			json.accumulate("ERROR","Error While Displaying Your Dashboard, Please Try Again");
		}	
		return json.toString();
	}
	
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 * 
	 * 
	 *  Created by Jeevan on November 24, 2015
	 *  Method to perform User Login..
	 * 
	 * 
	 */
	@RequestMapping("/login.do")
	public String loginUser(HttpServletRequest request,HttpServletResponse response,@RequestBody KikspotUserDto kikspotUserDto)throws Exception{
		log.info("inside loginUser() ");
		JSONObject json=new JSONObject();
		try{
			Integer userId=this.userService.authenticateMobileUser(kikspotUserDto);
			json.accumulate("STATUS", "Success");
			json.accumulate("UserId", userId);
		}
		catch(UserNotFoundException e){
			log.error(" NO User Found with given username "+e.toString());
			json.accumulate("ERROR", "No User Exists with Entered Username");
		}
		catch(PasswordNotMatchException e){
			log.error("Password Does Not match "+e.toString());
			json.accumulate("ERROR", "Invalid Password, Please Enter Again");
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return json.toString();
	}
	/**
	 * Created By Bhagya On November 26th, 2015
	 * @param request
	 * @param response
	 * @param kikspotUserDto
	 * @return
	 * @throws JSONException
	 * 
	 * Method For Updating the Profile Details
	 */
	
	@RequestMapping("/editprofile.do")
	public String editProfile(HttpServletRequest request,HttpServletResponse response,@RequestBody KikspotUserDto kikspotUserDto)throws JSONException{
		log.info("inside editProfile()");
		JSONObject json=new JSONObject();
		try{
			SimpleDateFormat dateFormat=new SimpleDateFormat("MM/dd/yyyy");
			kikspotUserDto.setDob(dateFormat.parse(kikspotUserDto.getDateofBirth()));
			Integer savedResult=this.userService.completeSignUp(kikspotUserDto);
			if(savedResult>0)
				json.accumulate("STATUS", "Profile Updated Successfully");			
			else
				throw new Exception();
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While Edit Profile");
			json.accumulate("ERROR", "Unexpected Error While Completing Edit Profile, Please Try Again");			
		}
		return json.toString();
	}
	

}
