package com.kikspot.frontend.mobile.controller;

import java.util.ArrayList;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;
import com.kikspot.frontend.common.utility.JsonParser;
import com.kikspot.frontend.game.dto.GameRulesDto;
import com.kikspot.frontend.game.dto.UserDashboardDto;
import com.kikspot.frontend.game.dto.UserGameCredsDto;
import com.kikspot.frontend.game.service.GameService;
import com.kikspot.frontend.user.service.UserService;


/***
 * 
 * 
 *  Created by Jeevan on November 27, 2015
 *  Controller for Game...
 * 
 * @author KNS-ACCONTS
 *
 */


@RestController
@RequestMapping("/mobile")
public class GameController {

	
	private static Logger log=Logger.getLogger(GameController.class);
	
	@Resource(name="userService")
	private UserService userService;
	
	@Resource(name="gameService")
	private GameService gameService;
	
	
	/**
	 * Created by Jeevan on November 27, 2015
	 * 
	 * 
	 * @param request
	 * @param response
	 * @param userId
	 * @return
	 * @throws JSONException 
	 */
	@RequestMapping("/dashboard.do")
	public String getUserDashboard(HttpServletRequest request,HttpServletResponse response) throws JSONException{
		log.info("inside getUserDashboard()");
		JSONObject json=new JSONObject();
		try{
			JSONObject inputJson=JsonParser.getJson(request, response);
			Integer userId=inputJson.getInt("userId");
			UserDashboardDto userDashboardDto=this.gameService.getUserDashboardInformation(userId);
		
			json.accumulate("USER",userDashboardDto.getKikspotUserDto().getFirstName());
			json.accumulate("CREDS", userDashboardDto.getUserGameCredDto().getCreds());
			json.accumulate("TRENDING", userDashboardDto.getTrending());
			ArrayList<JSONObject> topCredUsers=new ArrayList<JSONObject>();
			int i=1;
			for(UserGameCredsDto userGameCredDto:userDashboardDto.getUserGameCredsDtos()){
				JSONObject topCredUser=new JSONObject();
				topCredUser.accumulate("POSITION", i);
				topCredUser.accumulate("NAME", userGameCredDto.getKikspotUserDto().getFirstName());
				topCredUser.accumulate("CREDS",userGameCredDto.getCreds());
				topCredUsers.add(topCredUser);
				i++;
			}
			json.accumulate("TOPCREDUSERS",topCredUsers);
			ArrayList<JSONObject> gameRuleJsons=new ArrayList<JSONObject>();
			for(GameRulesDto gameRuleDto:userDashboardDto.getGameRulesDtos()){
				JSONObject gameRuleJson=new JSONObject();
				gameRuleJson.accumulate("RULE", gameRuleDto.getRule());
				gameRuleJsons.add(gameRuleJson);
			}
			json.accumulate("GAMERULES", gameRuleJsons);
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error While Fetching User Dashboard");
			json.accumulate("ERROR", "Error While Fetching Dashboard");
		}
		System.out.println(json.toString());
		return json.toString();
	}
}
