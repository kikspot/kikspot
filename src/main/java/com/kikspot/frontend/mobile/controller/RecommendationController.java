package com.kikspot.frontend.mobile.controller;

import java.util.ArrayList

;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;

import com.kikspot.backend.exceptions.NoResultsFoundException;
import com.kikspot.frontend.common.utility.JsonParser;
import com.kikspot.frontend.recommendation.dto.RecommendationLocationDto;
import com.kikspot.frontend.recommendation.service.RecommendationService;



/**
 * 
 *Created By Bhagya On November 23rd,2015
 * Controller class for the recommendation module and consists of mobile services
 */
@RestController
@RequestMapping("/mobile")
public class RecommendationController{
	private static Logger log=Logger.getLogger(RecommendationController.class);
	
	@Resource(name="recommendationService")
	private RecommendationService recommendationService;
	
	/**
	 * Created By Bhagya On November 23rd,2015
	 * @param request
	 * @param response
	 * @return
	 * Method For Returning the Basic Details Of Near By Places Based On Latitude and Longitude
	 * @throws JSONException 
	 */
	@RequestMapping(value="/getrecommendationlocations.do",method=RequestMethod.POST)
	public String getlocationsByLatAndLng(HttpServletRequest request,HttpServletResponse response,Map<String,Object> map) throws JSONException{
		log.info("inside  RecommendationController->getLocationByLatAndLng()");
		JSONObject output=new JSONObject();
		try{
			JSONObject json=JsonParser.getJson(request, response);
			Integer userId=json.getInt("userId");
			Double latitude=json.getDouble("latitude");
			Double longitude=json.getDouble("longitude");
			ArrayList<RecommendationLocationDto> recommendationLocationDtos=this.recommendationService.getLocations(userId, latitude, longitude);
			ArrayList<JSONObject> recommendationLocationJSONS=new ArrayList<JSONObject>();
			for(RecommendationLocationDto recommendationLocationDto:recommendationLocationDtos){
				JSONObject recommendationLocationJSON=new JSONObject();
				recommendationLocationJSON.accumulate("locationAPIId", recommendationLocationDto.getLocationAPIId());
				recommendationLocationJSON.accumulate("locationName", recommendationLocationDto.getLocationName());
				recommendationLocationJSON.accumulate("latitude", recommendationLocationDto.getLatitude());
				recommendationLocationJSON.accumulate("longitude", recommendationLocationDto.getLongitude());
				recommendationLocationJSON.accumulate("iconUrl", recommendationLocationDto.getIconUrl());
				recommendationLocationJSON.accumulate("distance", recommendationLocationDto.getDistance()+" Meters");
				recommendationLocationJSONS.add(recommendationLocationJSON);
			}
			
			output.accumulate("recommendationLocations", recommendationLocationJSONS);
			System.out.println(" Json Response"+output.toString());
			return output.toString();
		}
		catch(NoResultsFoundException e){
			//e.printStackTrace();
			String message="No Results Found For Search Criteria";
			output.accumulate("ERROR", message);
			System.out.println(" Json Response"+output.toString());
			return output.toString();
		}
		catch(Exception e){
			e.printStackTrace();
			String message="Error While Getting Recommendation Locations";
			output.accumulate("ERROR", message);
			return output.toString();
		}
	}
	/**
	 * Created By Bhagya On november 25th,2015
	 * @param request
	 * @param response
	 * @param map
	 * @return
	 * 
	 * Method For getting the Location Details By Location APIId
	 * @throws JSONException 
	 */
	@RequestMapping(value="/getrecommendationlocationdetails.do",method=RequestMethod.POST)
	public String getDetailsOfLocationByLocationAPIId(HttpServletRequest request,HttpServletResponse response,Map<String,Object> map) throws JSONException{
		log.info("inside RecommendationController->getDetailsOfLocationByLocationAPIId()");
		JSONObject output=new JSONObject();
		try{
			JSONObject json=JsonParser.getJson(request, response);
			String locationAPIId=json.getString("locationAPIId");
			RecommendationLocationDto recommendationLocationDto=this.recommendationService.getLocationDetailsByLocationAPIId(locationAPIId);
			JSONObject recommendationLocationJSON=new JSONObject();
			recommendationLocationJSON.accumulate("locationAPIId", recommendationLocationDto.getLocationAPIId());
			recommendationLocationJSON.accumulate("locationName", recommendationLocationDto.getLocationName());
			recommendationLocationJSON.accumulate("latitude", recommendationLocationDto.getLatitude());
			recommendationLocationJSON.accumulate("longitude", recommendationLocationDto.getLongitude());
			recommendationLocationJSON.accumulate("iconUrl", recommendationLocationDto.getIconUrl());
			recommendationLocationJSON.accumulate("image", recommendationLocationDto.getImage());
			recommendationLocationJSON.accumulate("ratings", recommendationLocationDto.getRatings());
			recommendationLocationJSON.accumulate("address", recommendationLocationDto.getAddress());
			recommendationLocationJSON.accumulate("phoneNo", recommendationLocationDto.getPhoneNo());
			recommendationLocationJSON.accumulate("isNew", recommendationLocationDto.getIsNew());
			recommendationLocationJSON.accumulate("url", recommendationLocationDto.getUrl());
			output.accumulate("recommendationLocationDetails", recommendationLocationJSON);
			System.out.println(" Json Response"+output.toString());
			return output.toString();
		}
		catch(NoResultsFoundException e){
			//e.printStackTrace();
			String message="No Results Found For Search Criteria";
			output.accumulate("ERROR", message);
			System.out.println(" Json Response"+output.toString());
			return output.toString();
		}
		catch(Exception e){
			e.printStackTrace();
			String message="Error While Getting  Location Details";
			output.accumulate("ERROR", message);
			return output.toString();
		}
	}
	
	
	
	
	
	
	
	
}